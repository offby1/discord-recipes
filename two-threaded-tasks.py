import collections
import decimal
import random
import threading
import time

running = True


class TimedTask:
    counter = 0

    def __init__(self, interval_seconds):
        self.interval_seconds = interval_seconds
        self.id = TimedTask.counter
        TimedTask.counter += 1
        self.total_calls = 0

    def __call__(self):
        while running:
            print(f"Task {self.id} here ({self.interval_seconds=})")
            time.sleep(float(self.interval_seconds))
            self.total_calls += 1


def main():
    global running
    total_seconds_to_run = 3

    task_callables = [TimedTask(decimal.Decimal(s / 10).quantize(decimal.Decimal('0.1'))) for s in random.sample(range(1, 31), k=10)]

    threads = [threading.Thread(target=f) for f in task_callables]
    for t in threads:
        t.start()
    time.sleep(total_seconds_to_run)
    running = False
    for t in threads:
        t.join()

    wat = collections.defaultdict(int)
    for tc in task_callables:
        wat[tc.interval_seconds] += tc.total_calls

    for delay_seconds, number_of_hits in wat.items():
        print(f"{delay_seconds=} * {number_of_hits=} ({delay_seconds * number_of_hits}) should be close to {total_seconds_to_run=}")


main()
