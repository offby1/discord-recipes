# Reminiscent of, but much simpler than, https://en.wikipedia.org/wiki/Job_shop_scheduling

import dataclasses
import functools

# pip install gibberish
import gibberish  # type: ignore

import random
import statistics
import typing


@functools.total_ordering
@dataclasses.dataclass
class Shift:
    description: str
    length_in_hours: int

    @classmethod
    def random_shift(klass):
        shift_lengths = (0.25, 0.5, 1, 1.5, 2, 2.5, 3, 5, 6, 8)
        weights = (1, 2, 5, 1, 5, 1, 2, 1, 1, 1)
        chosen_length = random.choices(population=shift_lengths, weights=weights)[0]
        return klass(
            description=f"A {chosen_length}-hour shift", length_in_hours=chosen_length
        )

    @staticmethod
    def sum_shifts(shifts):
        return sum(s.length_in_hours for s in shifts)

    def __lt__(self, other):
        return self.length_in_hours < other.length_in_hours

    def __eq__(self, other):
        return self.length_in_hours == other.length_in_hours


@dataclasses.dataclass
class Worker:
    name: str
    shifts: typing.List[Shift] = dataclasses.field(default_factory=list)

    def total_hours(self):
        return Shift.sum_shifts(self.shifts)

    def __repr__(self):
        description = " + ".join([str(s.length_in_hours) for s in self.shifts])
        description = f"{self.total_hours()} total hours ({description})"
        return f"{self.__class__} {self.name}: {description}"

    @classmethod
    def random_worker(klass):
        gibberish_generator = gibberish.Gibberish()
        return klass(
            name=" ".join([w.title() for w in gibberish_generator.generate_words(2)])
        )


def generate_random_shifts():
    unassigned_shifts: typing.List[Shift] = []

    # Fill up the week.
    while True:
        unassigned_shifts.append(Shift.random_shift())
        total = Shift.sum_shifts(unassigned_shifts)
        if total >= 168:
            excess = total - 168
            last_shift = unassigned_shifts[-1]
            last_shift.length_in_hours -= excess
            assert last_shift.length_in_hours > 0
            break

    return unassigned_shifts


def assign_shifts_to_workers(workers, unassigned_shifts):
    for shift in sorted(unassigned_shifts, reverse=True):

        emptiest_worker = min(workers, key=lambda w: w.total_hours())
        emptiest_worker.shifts.append(shift)


def main():
    unassigned_shifts = generate_random_shifts()

    workers = [Worker.random_worker() for _ in range(20)]
    assign_shifts_to_workers(workers, unassigned_shifts)
    return workers


if __name__ == "__main__":
    workers = main()

    worker_hours = [w.total_hours() for w in workers]
    print(f"Total hours assigned to workers: {sum(worker_hours)}")
    print(f"Mean hours per worker: {statistics.mean(worker_hours)}")
    print(f"Most-slackulous worker: {min(workers, key=lambda w: w.total_hours())}")
    print(f"Most-virtuous worker: {max(workers, key=lambda w: w.total_hours())}")
