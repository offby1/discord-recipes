import collections
import pprint
import random
import time

import graphviz  # pip install graphviz
import more_itertools  # pip install more_itertools
import names  # pip install names


class Person:
    all_persons = []
    persons_by_chromosones = collections.defaultdict(set)
    the_big_family_tree = graphviz.Digraph(
        "The Big Family Tree",
        edge_attr={"dir": "None"},
        graph_attr={"splines": "ortho"},
        node_attr={"shape": "box"},
    )

    def __init__(self, parents=None):
        if parents is None:
            self.chromosones = [object() for _ in range(23)]
        else:
            assert len(set(parents)) == 2
            assert sum([p.i_am_male for p in parents]) == 1
            self.mama = parents[0]
            self.papa = parents[1]
            self.chromosones = [
                random.choice([c[0], c[1]])
                for c in zip(self.mama.chromosones, self.papa.chromosones)
            ]

        for c in self.chromosones:
            self.persons_by_chromosones[c].add(self)

        self.i_am_male = len(self.all_persons) % 2
        gender = "male" if self.i_am_male else "female"
        if (papa := getattr(self, "papa", None)) is not None:
            self.name = (
                names.get_first_name(gender=gender) + " " + papa.name.split()[-1]
            )
        else:
            self.name = names.get_full_name(gender=gender)

        self.all_persons.append(self)
        self.the_big_family_tree.node(self.name)  # todo -- protect against duplicates
        if hasattr(self, "papa"):
            self.the_big_family_tree.edge(self.papa.name, self.name)
        if hasattr(self, "mama"):
            self.the_big_family_tree.edge(self.mama.name, self.name)

    @property
    def shortname(self):
        return f"{self.name} {'m' if self.i_am_male else 'f'}"

    def __str__(self):
        rv = self.shortname
        if hasattr(self, "mama"):
            rv += f" {self.mama.shortname} {self.papa.shortname}"
        return rv

    def __repr__(self):
        return f"<{self.shortname}>"

    def document_ancestry(self):
        chromosone_count_by_ancestor = collections.Counter()
        for c in self.chromosones:
            for ancestor in self.persons_by_chromosones[c]:
                if ancestor is not self:
                    chromosone_count_by_ancestor[ancestor] += 1

        return {a: chromosone_count_by_ancestor[a] for a in self.all_persons}


lotta_parents = [Person() for _ in range(32)]
while len(lotta_parents) >= 2:
    next_generation = []
    for chunk in more_itertools.chunked(lotta_parents, 2):
        next_generation.append(Person(chunk))
    lotta_parents = next_generation

pprint.pprint(lotta_parents[0].document_ancestry())

# Note:
#     There is no option to wait for the application to close,
#     and no way to retrieve the application's exit status.

# Further ideas from https://www.google.com/search?q=graphviz+family+tree:
# in particular https://stackoverflow.com/questions/2271704/family-tree-layout-with-dot-graphviz
Person.the_big_family_tree.unflatten(stagger=10).view()
while True:
    time.sleep(10)
