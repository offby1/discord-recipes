import itertools


def longest_common_prefix(w1, w2):
    while True:
        if not w1:
            return
        if not w2:
            return
        if not w1[0] == w2[0]:
            return
        yield w1[0]
        w1 = w1[1:]
        w2 = w2[1:]


def all_suffixes(w):
    while True:
        if not w:
            return
        yield w
        w = w[1:]


def all_combos(w1, w2):
    return itertools.product(all_suffixes(w1), all_suffixes(w2))


def longest_common_substring(w1, w2):
    so_far = ""
    for w1, w2 in all_combos(w1, w2):
        lcp = "".join(longest_common_prefix(w1, w2))
        if len(lcp) > len(so_far):
            print(f"{lcp=} > {so_far=}")
            so_far = lcp

    return so_far


print(longest_common_substring("abcde", "xbcdey"))
print(longest_common_substring("abofrotzbananawhatever", "bobanana"))
