import unicodedata

message = """THE HAIRY HANDED GENT
WHO RAN AMOK IN KENT
I'D LIKE TO MEET HIS TAILOR"""

translated = []
for ch in message:
    name = f"REGIONAL INDICATOR SYMBOL LETTER {ch}"

    try:
        new = unicodedata.lookup(name)
    except KeyError:
        new = ch

    translated.append(new)


print(unicodedata.lookup("ZERO WIDTH SPACE").join(translated))
