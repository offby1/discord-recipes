# https://en.wikipedia.org/wiki/Flag_of_the_United_States#Specifications

import math
import turtle
from cmath import pi, rect
from typing import Iterator

A_hoist = 300  # pixels
B_fly = A_hoist * 1.9
C_canton_height = 7 / 13 * A_hoist
D_canton_width = 2 / 5 * B_fly
F_star_grid_cell_height = C_canton_height / 10
H_star_grid_cell_width = D_canton_width / 12
L_stripe_width = A_hoist / 13
K_star_diameter = L_stripe_width * 4 / 5

RED = (178 / 255, 34 / 255, 52 / 255)
WHITE = (1, 1, 1)
BLUE = (60 / 255, 59 / 255, 110 / 255)


def star_coordinates() -> Iterator[tuple[float, float]]:
    big_radius = K_star_diameter / 2
    small_radius = K_star_diameter / 2 * math.sin(pi * 0.1) / math.sin(pi * 0.7)

    for point_index in range(10):
        radius = big_radius if point_index % 2 == 0 else small_radius

        phi = 2 * pi * (1 / 10 * point_index)
        z = rect(radius, phi)
        yield z.real, z.imag


def white_box() -> None:
    t.color(*WHITE)
    t.penup()
    t.goto(0, 0)
    t.pendown()
    t.begin_fill()
    t.forward(B_fly)
    t.left(90)
    t.forward(A_hoist)
    t.left(90)
    t.forward(B_fly)
    t.end_fill()


def red_stripes() -> None:
    for stripe_index in range(0, 13, 2):
        ll_x = 0
        ll_y = stripe_index * L_stripe_width
        t.color(*RED)
        t.penup()
        t.goto(ll_x, ll_y)
        t.pendown()
        t.begin_fill()
        t.setheading(0)
        t.forward(B_fly)
        t.left(90)
        t.forward(L_stripe_width)
        t.left(90)
        t.forward(B_fly)
        t.end_fill()


def canton() -> None:
    t.color(*BLUE)
    t.penup()
    t.goto(0, 6 * L_stripe_width)
    t.pendown()
    t.begin_fill()
    t.setheading(0)
    t.forward(D_canton_width)
    t.left(90)
    t.forward(C_canton_height)
    t.left(90)
    t.forward(D_canton_width)
    t.end_fill()


def stars() -> None:
    def row_at(x: float, y: float, *, number_of_stars: int) -> None:
        for s in range(number_of_stars):
            t.color(*WHITE)
            t.penup()
            t.goto(x + s * H_star_grid_cell_width * 2, y)
            t.stamp()

    for row in range(9):
        x = H_star_grid_cell_width
        y = 6 * L_stripe_width + (row + 1) * F_star_grid_cell_height

        if row % 2 == 1:
            row_at(x + H_star_grid_cell_width, y, number_of_stars=5)
        else:
            row_at(x, y, number_of_stars=6)


def flag() -> None:
    white_box()
    red_stripes()
    canton()
    stars()


if __name__ == "__main__":
    turtle.setworldcoordinates(0, -B_fly / 4, B_fly, 3 * B_fly / 4)
    turtle.register_shape("star", tuple(star_coordinates()))

    t = turtle.Turtle()
    t.shape("star")
    t.speed(0)
    turtle.Screen().bgcolor("black")

    flag()
    turtle.exitonclick()
