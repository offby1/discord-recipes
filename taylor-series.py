# https://en.wikipedia.org/wiki/Taylor_series#Trigonometric_functions

import functools
import math


@functools.lru_cache
def fact(n):
    product = 1

    for x in range(2, n + 1):
        product *= x

    return product


def taylor_sum(x, term_func, nterms):
    sum = 0
    for n in range(0, nterms):
        term = term_func(n, x)
        sum += term
    return sum


def sin(x, nterms=50):
    def term_func(n, x):
        minus_one_to_the_n = (-1) ** n
        two_n_plus_one = 2 * n + 1
        return (minus_one_to_the_n / fact(two_n_plus_one)) * (x ** two_n_plus_one)

    return taylor_sum(x, term_func, nterms)


def cos(x, nterms=50):
    def term_func(n, x):
        minus_one_to_the_n = (-1) ** n
        two_n = 2 * n
        return (minus_one_to_the_n / fact(two_n)) * (x ** two_n)

    return taylor_sum(x, term_func, nterms)


def arctan(x, nterms=500):
    def term_func(n, x):
        minus_one_to_the_n = (-1) ** n
        two_n_plus_one = 2 * n + 1
        return (minus_one_to_the_n / two_n_plus_one) * (x ** two_n_plus_one)

    return taylor_sum(x, term_func, nterms)


if __name__ == "__main__":
    print(f"{sin(45 / 180 * math.pi) ** 2=}")
    print(f"{4 * arctan(1)=}")
