import itertools

from tabulate import tabulate

plays = [
    'rock', 'paper', 'scissors', 'spock', 'lizard',
]


def winner(his_play, her_play):
    if his_play == her_play:
        return None

    c = itertools.cycle(plays)
    while True:
        candidate = next(c)
        if candidate == his_play:
            distance = 0
            break

    while candidate != her_play:
        candidate = next(c)
        distance += 1

    if distance % 2 == 1:
        return her_play

    return his_play


def all_combos():
    for him, her in itertools.product(plays, repeat=2):
        the_winner = winner(him, her)
        if the_winner is None:
            the_winner = 'tie'
        yield (him, 'vs', her, '=>', the_winner)


if __name__ == "__main__":
    import operator
    print(tabulate(sorted(all_combos(), key=operator.itemgetter(-1, 0))))
