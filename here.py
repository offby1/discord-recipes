import builtins
import functools
import inspect
import pathlib


def my_directory():
    caller_frame = inspect.stack()[1]
    caller_filename = caller_frame.filename

    here = pathlib.Path(caller_filename).parent

    print(f"{caller_filename=} {here=}")

    return here.absolute()


def install_my_open():
    global open

    @functools.wraps(builtins.open)
    def my_open(filename, **kwargs):
        try:
            return builtins.open(filename, **kwargs)
        except FileNotFoundError as e:
            if hasattr(e, "add_note"):
                filename = pathlib.Path(filename)
                if not filename.is_absolute():
                    here = pathlib.Path().absolute()
                    description = f"Looked in current working directory {here}\nfor {here / filename}"
                    e.add_note(description)
                raise e from None
            else:
                raise

    open = my_open


if __name__ == "__main__":
    install_my_open()
    open("frobotz")
