from hypothesis import given, strategies as st


def insert_item(item, sorted_seq):

    result = []

    print(f"{item=} {sorted_seq=}")

    while True:
        if not sorted_seq:
            result.append(item)
            print(f"input is empty, so {result=}")
            return result

        if item <= sorted_seq[0]:
            result.append(item)
            result.extend(sorted_seq)
            print(f"{item} is not greater than {sorted_seq[0]}, so {result=}")
            return result

        result.append(sorted_seq[0])
        sorted_seq = sorted_seq[1:]
        print(f"input isn't empty, and {item} isn't smaller than {result[-1]}; still loopin'; {result=} {sorted_seq=}")


@given(st.integers(), st.lists(st.integers()).map(sorted))
def test_em_all(i, lst):
    assert insert_item(i, lst) == list(sorted([i] + lst))


def insertion_sort(seq):
    result = []
    while seq:
        result = insert_item(seq[0], result)
        seq = seq[1:]
    return result


if __name__ == "__main__":
    import random

    for _ in range(10):
        lst = random.choices(range(20), k=20)
        assert sorted(lst) == insertion_sort(lst)
