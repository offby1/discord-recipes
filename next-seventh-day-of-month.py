import datetime


def next_seventh_of_month(starting_from=None):
    if starting_from is None:
        starting_from = datetime.date.today()

    seventh = starting_from.replace(day=7)
    while True:
        if seventh.day == 7 and seventh >= starting_from:
            break
        seventh += datetime.timedelta(days=1)

    return seventh
