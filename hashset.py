from typing import Iterator


class HashSet:
    def __init__(self, num_buckets: int = 43) -> None:
        self.buckets: list[list[str]] = [[] for _ in range(num_buckets)]

    def _compute_hash_code(self, item: str) -> int:
        hash_value = 0
        for char in item:
            hash_value = hash_value * 31 + ord(char)
        return hash_value

    def _bucket_for_item(self, item: str) -> list[str]:
        hash_code = self._compute_hash_code(item)
        bucket_index = hash_code % len(self.buckets)
        return self.buckets[bucket_index]

    def add_item(self, item: str) -> None:
        bucket = self._bucket_for_item(item)
        if item not in bucket:
            bucket.append(item)

    def is_item_present(self, item: str) -> bool:
        bucket = self._bucket_for_item(item)
        return item in bucket

    def all_items(self) -> Iterator[str]:
        for b in self.buckets:
            yield from b


def test_empty() -> None:
    hs = HashSet()
    assert not hs.is_item_present("fred")
    assert next(hs.all_items(), None) is None


def test_insertion() -> None:
    hs = HashSet()
    hs.add_item("fred")
    assert hs.is_item_present("fred")
    assert list(hs.all_items()) == ["fred"]
