* bytes => hex => hex-encoded string

        >>> b'ah one and a two'.hex()
        '6168206f6e6520616e6420612074776f'

* bytes <= fromhex <= hex-encoded string

        >>> bytes.fromhex('6168206f6e6520616e6420612074776f')
        b'ah one and a two'

* int => '{:02x}'.format(...) => hex-encoded string

        >>> '{:x}'.format(129475772330023423182976676189068097391)
        '6168206f6e6520616e6420612074776f'

* int => hex => hex-encoded string

        >>> hex(129475772330023423182976676189068097391)
        '0x6168206f6e6520616e6420612074776f'

* int <= int(..., 16) <= hex-encoded string

        >>> int('6168206f6e6520616e6420612074776f', 16)
        129475772330023423182976676189068097391

* int => to_bytes => bytes

        >>> (129475772330023423182976676189068097391).to_bytes(25, 'big').lstrip(b'\x00')
        b'ah one and a two'

        >>> (129475772330023423182976676189068097391).to_bytes(16, 'little')
        b'owt a dna eno ha'

* int =< from_bytes <= bytes

        >>> int.from_bytes(b'ah one and a two', 'big')
        129475772330023423182976676189068097391

        >>> int.from_bytes(b'ah one and a two', 'little')
        148164546183165736282032673847236388961

`to_bytes` requires both a size and an endianness indicator, whereas you can sort of do

    int_to_bytes = lambda i: bytes.fromhex(('{:x}'.format(i)))

except that only works for values >= 16

    >>> int_to_bytes(15)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "<stdin>", line 1, in <lambda>
    ValueError: non-hexadecimal number found in fromhex() arg at position 1
    >>> int_to_bytes(16)
    b'\x10'

* uuids, ints, bytes

        import uuid

        u = uuid.uuid4()
        print(u)

        big = int.from_bytes(u.bytes, 'big')
        print(big)
        again = uuid.UUID(int=big)
        print(again)
