# Lose the .env file?  Go to https://www.reddit.com/prefs/apps, scroll to the bottom, click "Edit" in the "wat"
# section, scrape out the sekrit
# Similarlly for the client id: it's in the upper-left.

import os
import sys

import dotenv  # pip install python-dotenv
import praw  # pip install praw

dotenv.load_dotenv()

reddit = praw.Reddit(
    client_id=os.environ["REDDIT_CLIENT_ID"],
    client_secret=os.environ["REDDIT_SEKRIT"],
    user_agent=f"{sys.platform}:wat:0.0 (by u/offby1)",
)


for submission in reddit.subreddit("learnpython").hot(limit=10):
    print(submission.title)
