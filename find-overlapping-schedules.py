import dataclasses
import functools

some_ranges = [
    (1, 2),
    (3, 4),
    (5, 10),
    (2, 3.5),
    (11, 20),
]


@dataclasses.dataclass
class Range:
    start: int
    stop: int
    id: int


@functools.total_ordering
@dataclasses.dataclass(kw_only=True)
class Event:
    time: int
    id: int
    is_start: bool
    associated_range: Range

    def __repr__(self):
        return f"{'start' if self.is_start else 'end'} of {self.associated_range} at time {self.time}"

    def __lt__(self, other):
        return self.time < other.time


annotated_ranges = [Range(*t, id=index) for index, t in enumerate(some_ranges)]
events = []
for r in annotated_ranges:
    events.append(Event(time=r.start, id=r.id, is_start=True, associated_range=r))
    events.append(Event(time=r.stop, id=r.id, is_start=False, associated_range=r))

events = sorted(events)

# find overlaps
currently_running = None
for e in events:
    if e.is_start:
        if currently_running and currently_running.id != e.id:
            print(
                f"{currently_running.associated_range} overlaps {e.associated_range} at time {e.time}",
            )
            break
        currently_running = e
    else:
        currently_running = None
else:
    print("No overlaps")
