import trio


async def producer(send_ch):
    async with send_ch:
        for numba in range(100):
            await send_ch.send(numba)


def make_consumer(sleepy_time):
    async def consumer(receive_ch):
        messages_received = 0
        async for value in receive_ch:
            messages_received += 1
            print(f"consumer {sleepy_time}: got {value}")
            await trio.sleep(sleepy_time)
        print(f"consumer {sleepy_time}: bowing out after having received {messages_received} messages")
    return consumer


async def producer_consumer():
    send_ch, receive_ch = trio.open_memory_channel(0) # like a queue, but each end is a separate value
    async with send_ch, receive_ch:
        async with trio.open_nursery() as nursery:
            nursery.start_soon(producer, send_ch)
            for sleepy_time in (0.02, 0.1):
                nursery.start_soon(make_consumer(sleepy_time), receive_ch)

trio.run(producer_consumer)
