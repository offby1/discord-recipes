import dataclasses
import operator
from typing import FrozenSet, List, Set

# python3 -m pip install inflect
import inflect  # type: ignore
import tabulate

Inflector = inflect.engine()


def _andify(words: List[str]) -> str:
    butlast = words[:-1]
    last = words[-1]

    if not butlast:
        return last

    if len(butlast) == 1:
        return f"{butlast[0]} and {last}"

    return ", ".join(butlast) + ", and " + last


def _coin_name(denomination_in_cents: int) -> str:
    if conventional_name := {
        1: "penny",
        5: "nickel",
        10: "dime",
        25: "quarter",
        50: "half-dollar",
        100: "dollar",
    }.get(denomination_in_cents):
        return conventional_name

    return f"{denomination_in_cents}-cent piece"


def _pluralize(noun: str, quantity: int) -> str:
    if quantity != 1:
        return f"{quantity} {Inflector.plural(noun)}"
    return f"1 {noun}"


@dataclasses.dataclass
class PileOfLikeCoins:
    quantity: int
    denomination: int

    def __str__(self) -> str:
        word = _coin_name(self.denomination)
        return _pluralize(word, self.quantity)

    def __repr__(self) -> str:
        return str(self)

    def __hash__(self) -> int:
        return hash((self.quantity, self.denomination))

    @property
    def value(self):
        return self.quantity * self.denomination


def dollars_and_cents_string(cents: int) -> str:
    dollars, cents = divmod(cents, 100)
    return f"${dollars}.{cents:02d}"


OneSolution = FrozenSet[PileOfLikeCoins]
AllSolutions = Set[OneSolution]


# This feels almost identical to computing anagrams
def ways_to_make_change(denominations: Set[int], desired_sum: int) -> AllSolutions:
    result: AllSolutions = set()

    if not denominations:
        return result

    if desired_sum <= 0:
        return result

    for candidate in denominations:
        this_candidate_result: AllSolutions = set()

        q, r = divmod(desired_sum, candidate)
        if r == 0:
            result.add(frozenset([PileOfLikeCoins(quantity=q, denomination=candidate)]))

        while q > 0:
            smaller_denomination_set = denominations - set([candidate])

            from_recursive_call = ways_to_make_change(
                smaller_denomination_set, desired_sum - candidate * q
            )

            this_candidate_result = this_candidate_result.union(
                [
                    piles.union(
                        frozenset(
                            [PileOfLikeCoins(quantity=q, denomination=candidate)]
                        )
                    )
                    for piles in from_recursive_call
                ]
            )

            result.update(this_candidate_result)

            q -= 1

    return result


if __name__ == "__main__":
    for coins, desired_sum in (
        ([1, 5, 10, 25, 50, 100], 191),
        ([5, 10], 100),
        ([10, 25], 100),
        ([1, 5, 50], 273),
    ):
        pluralized_coin_names = [Inflector.plural(_coin_name(c)) for c in coins]

        print(
            f"\nWays to make change of {dollars_and_cents_string(desired_sum)} from {_andify(pluralized_coin_names)}:"
        )
        all_ways = sorted(
            ways_to_make_change(set(coins), desired_sum),
            key=lambda w: sum(p.quantity for p in w),
        )
        table = []
        for w in all_ways:
            assert desired_sum == sum(c.value for c in w)
            w_sorted = sorted(w, key=operator.attrgetter("denomination"))
            description = map(str, w_sorted)
            n_coins = sum(pile.quantity for pile in w_sorted)
            table.append((_pluralize("coin", n_coins), "; ".join(description)))
        print(
            tabulate.tabulate(
                table, headers=["total coins", "breakdown"], tablefmt="orgtbl"
            )
        )
