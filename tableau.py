import itertools


def _make_tableau(seq):
    length = len(seq)
    cycle = itertools.cycle(seq)

    for i in range(length):
        yield list(itertools.islice(cycle, length))
        next(cycle)


if __name__ == "__main__":
    t = _make_tableau  ("hello world")
    for row in t:
        print (''.join(row))
