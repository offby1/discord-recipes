import queue
import threading


def ask_with_timeout(question, timeout_seconds):
    q = queue.Queue()

    def collect_answer():
        q.put(input(question + "? "))

    t = threading.Thread(target=collect_answer)
    t.start()

    try:
        return q.get(block=True, timeout=timeout_seconds)
    except queue.Empty:
        return None


questions = [
    "who are you",
    "what is your quest",
    "what is the airspeed of an unladen swallow",
]

for q in questions:
    response = ask_with_timeout(q, 3)
    if response is not None:
        print(f"{q} => {response}")
    else:
        print(f"OK, you skipped {q}; fine")
