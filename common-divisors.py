import sympy.ntheory


def find_common_divisors(*args):
    return sorted(set.intersection(*(set(sympy.ntheory.divisors(arg)) for arg in args)))


assert find_common_divisors(7920, 8000, 4000) == [1, 2, 4, 5, 8, 10, 16, 20, 40, 80]
