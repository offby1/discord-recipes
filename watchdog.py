import os
import subprocess
import time


def create_assassin(victim_pid):
    print(subprocess.Popen(
        [
            "/bin/sh",
            "-c",
            f"""
    sleep 10
    kill {victim_pid}
    """,
        ]
    ))


create_assassin(os.getpid())

while True:
    print("main process doing work.")
    time.sleep(2)
