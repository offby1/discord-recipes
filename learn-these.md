Stuff I don't know about, but that people on discord are constantly asking about:

- screen-scraping (selenium, beautiful soup)
- pandas & dataframes
- numpy
- matplotlib

Stuff I know a bit about but could know more:

- concurrency
- async in general
- discord bots
- the more popular IDEs (vscode and pycharm)
