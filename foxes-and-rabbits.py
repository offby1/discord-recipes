rabbits = 10
foxes = 10

while (rabbits + foxes) > 0:
    print(f"There are {rabbits=} and {foxes=}")
    # foxes eat rabbits sometimes
    hungry_foxes = int(round(foxes * 0.3325))

    print(f"{hungry_foxes} foxes are hungry")

    # foxes die if not enough rabbits
    if hungry_foxes > rabbits:
        foxes = hungry_foxes = rabbits
        print(f"Not enough rabbits; now there are {foxes=}")

    rabbits -= hungry_foxes
    print(f"{hungry_foxes=} ate some rabbits; leaving {rabbits=}")

    # other causes
    foxes = int(round(foxes * 0.9))
    rabbits = int(round(rabbits * 0.9))
    print(f"Nature kills; now {rabbits=} and {foxes=}")

    # Sexy time
    foxes = int(round(foxes * 1.2))
    rabbits = int(round(rabbits * 1.8))
    print(f"Sexy time: now {rabbits=} and {foxes=}")
