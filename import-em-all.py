import importlib
import pathlib
import sys


def importable_files(directory):
    for f in sorted(directory.iterdir()):
        if f.name.endswith('.py'):
            name = f.with_suffix('').name
            yield (name)


if __name__ == "__main__":
    MODULE_DIRECTORY = pathlib.Path('/tmp')

    sys.path.insert(0, str(MODULE_DIRECTORY))

    for module_name in importable_files(MODULE_DIRECTORY):
        try:
            m = importlib.import_module(module_name)
        except Exception as e:
            print(f"Durn it, can't import {module_name} because {e}")
            continue

        globals()[m.__name__] = m
        print(f"Imported {m}")
