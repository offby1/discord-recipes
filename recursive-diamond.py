def triangle(*, height, scoot_over_ness=0, inverse=False):
    def t():
        if height > 1:
            triangle(
                height=height - 1,
                scoot_over_ness=scoot_over_ness + 1,
                inverse=inverse,
            )

    if not inverse:
        t()
    print(" " * scoot_over_ness + "*" * (height * 2 - 1))
    if inverse:
        t()


if __name__ == "__main__":
    triangle(height=10)
    triangle(height=10, inverse=True)
