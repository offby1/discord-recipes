def num2words(n):
    n = abs(int(round(n)))  # make it simple, to start

    if n < 10:
        return {
            0: "zero",
            1: "one",
            2: "two",
            3: "three",
            4: "four",
            5: "five",
            6: "six",
            7: "seven",
            8: "eight",
            9: "nine",
        }[n]

    if n < 100:
        words = []
        tens, units = divmod(n, 10)

        if tens == 1:
            return {
                0: "ten",
                1: "eleven",
                2: "twelve",
                3: "thirteen",
                4: "fourteen",
                5: "fifteen",
                6: "sixteen",
                7: "seventeen",
                8: "eighteen",
                9: "nineteen",
            }[units]
        else:
            words.append(
                {2: "twenty", 3: "thirty", 4: "forty", 5: "fifty", 6: "sixty", 7: "seventy", 8: "eighty", 9: "ninety",}[
                    tens
                ]
            )

            if units > 0:
                words.append(num2words(units))

        return '-'.join(words)

    if n < 1000:
        hundreds, rest = divmod(n, 100)
        return f"{num2words(hundreds)} hundred and {num2words(rest)}"

    if n < 10000:
        thousands, rest = divmod(n, 1000)
        return f"{num2words(thousands)} thousand, {num2words(rest)}"


def test_zero():
    assert "zero" == num2words(0)


def test_seven():
    assert "seven" == num2words(7)


def test_ten():
    assert "ten" == num2words(10)


def test_eleven():
    assert "eleven" == num2words(11)


def test_sixty_five():
    assert "sixty-five" == num2words(65)


if __name__ == "__main__":
    for n in range(1001, 10000, 103):
        print(f"{n=} {num2words(n)}")
