#!/bin/sh

set -ex

python3 -m pip install poetry

rm -rf /tmp/wat
mkdir  /tmp/wat
cd     /tmp/wat

python3 -m poetry.console.application init --no-interaction
python3 -m poetry.console.application add "imgui[pygame]"
python3 -m poetry.console.application show --verbose

ls -l .venv/lib/python*/site-packages
