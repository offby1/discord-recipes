import collections
import pprint
import re


def words_and_line_numbers(lines):
    for line_number, line in enumerate(lines):
        line_number += 1
        line = line.rstrip()
        for word in re.split(r'[^A-Za-z]', line):
            if word:
                yield (word, line_number)


def concordance_from_file(filename):
    raw_concordance = collections.defaultdict(set)

    with open(filename) as inf:
        for word, line_number in words_and_line_numbers(inf):
            raw_concordance[word].add(line_number)

    with_sorted_line_numbers = {}

    for word, lines in sorted(raw_concordance.items(), key=lambda k_v: k_v[0].lower()):
        with_sorted_line_numbers[word] = sorted(lines)

    return with_sorted_line_numbers


if __name__ == "__main__":
    pprint.pprint(concordance_from_file('/etc/passwd'))
