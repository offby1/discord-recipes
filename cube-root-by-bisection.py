import tabulate


def cube_root_by_bisection(n):

    low_guess = 1
    high_guess = n

    while True:
        mid_guess = (low_guess + high_guess) / 2
        wat = pow(mid_guess, 3)
        yield low_guess, mid_guess, high_guess, wat
        if abs(wat - n) < 0.01:
            return

        if wat > n:
            high_guess = mid_guess
        else:
            low_guess = mid_guess


for index, n in enumerate((8, 1000)):
    if index > 0:
        print()
    print(n)
    print(tabulate.tabulate(cube_root_by_bisection(n), headers=["low", "mid", "high", "target"]))
