# https://xkcd.com/2483/

class Node:
    def __init__(self, datum):
        self.datum = datum
        self.next_node = None


class LinkedList:
    def __init__(self):
        self.head_node = None

    def append(self, datum):
        n = Node(datum)
        if self.head_node is None:
            self.head_node = n
        else:
            tail = self.head_node
            while tail.next_node is not None:
                tail = tail.next_node

            tail.next_node = n

    def prepend(self, datum):
        n = Node(datum)
        n.next_node = self.head_node
        self.head_node = n

    def __repr__(self):
        nodes = []
        tail = self.head_node
        while tail is not None:
            nodes.append(str(tail.datum))
            tail = tail.next_node

        return f'<{", ".join(nodes)}>'


if __name__ == "__main__":
    ll = LinkedList()
    print(ll)
    ll.append(1)
    ll.append(2)
    print(ll)
    ll.prepend(-10)
    ll.prepend(-20)
    print(ll)
