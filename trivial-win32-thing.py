from win32.win32gui import EnumChildWindows  # py -3 -m pip install pywin32
from win32.win32gui import GetWindowText, IsWindowVisible


def winEnumHandler(hwnd, ctx):
    if IsWindowVisible(hwnd):
        print(hex(hwnd), GetWindowText(hwnd))


EnumChildWindows(0, winEnumHandler, None)
