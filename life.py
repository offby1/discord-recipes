# https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
import curses
import datetime
import itertools
import time
from typing import Iterable, Optional

import life_seeds

# It might be worth investigating https://pypi.org/project/blessings/
# or http://urwid.org/tutorial/ as alternatives to curses.

# a pygame example: https://raw.githubusercontent.com/Marvelman3284/Sci-Project/master/life.py
# another: https://github.com/salt-die/tiny-math/blob/master/game_of_life_averages.py
wraparound = True


class Cell:
    def __init__(self) -> None:
        self.current_status: bool = False
        self.future_status: Optional[bool] = None
        self.age: int = 0

    def set_future_status_from_neighbors(self, neighbors: Iterable["Cell"]) -> None:
        sum_of_living_neighbors = sum([1 if n.current_status else 0 for n in neighbors])

        self.future_status = False

        if sum_of_living_neighbors == 3:
            self.future_status = True
        elif sum_of_living_neighbors == 2 and self.current_status:
            self.future_status = True

    def set_current_status_from_future_status(self) -> None:
        assert self.future_status is not None
        self.age += 1
        if self.current_status != self.future_status:
            self.age = 0
        self.current_status = self.future_status
        self.future_status = None

    def __str__(self) -> str:
        return "#" if self.current_status else " "

    def __repr__(self) -> str:
        return str(self)


class Board:
    def __init__(self, num_rows: int, num_cols: int) -> None:
        self.rows = [[Cell() for _ in range(num_cols)] for _ in range(num_rows)]

    @property
    def height(self) -> int:
        return len(self.rows)

    @property
    def width(self) -> int:
        return len(self.rows[0])

    def add_seed(self, seed_string: str) -> None:
        seed_rows = seed_string.split("\n")
        seed_width = len(seed_rows[0])
        seed_height = len(seed_rows)

        h_offset = (self.width - seed_width) // 2
        v_offset = (self.height - seed_height) // 2

        for row_index, row in enumerate(seed_rows):
            for ch_index, ch in enumerate(row):
                self.rows[row_index + v_offset][ch_index + h_offset].current_status = (
                    ch == "#"
                )

    def update(self) -> None:
        # Figure out what will happen at the next generation.
        for row_index, row in enumerate(self.rows):
            for column_index, cell in enumerate(row):
                neighbors = [
                    self.rows[index[0]][index[1]]
                    for index in neighboring_indices(
                        self.width, self.height, row_index, column_index
                    )
                ]
                cell.set_future_status_from_neighbors(neighbors)

        # OK, now it _is_ the next generation.
        for row_index, row in enumerate(self.rows):
            for column_index, cell in enumerate(row):
                cell.set_current_status_from_future_status()

    def draw(self, stdscr: curses.window, center_label: str) -> None:
        # TODO -- obviously it'd be faster if we only updated those cells that changed since the last go-round.
        for row_index, row in enumerate(self.rows):
            for column_index, cell in enumerate(row):
                # Crazy as it sounds, you get an exception if you write to this lower-right corner cell.
                # Says so right in the docs, too -- .../library/curses.html#curses.window.addstr
                if (row_index == self.height - 1) and (column_index == self.width - 1):
                    pass
                elif cell.age == 0:
                    stdscr.addstr(row_index, column_index, str(cell))

        center_x, center_y = (self.width - len(center_label)) // 2, self.height // 2
        stdscr.addstr(center_y, center_x, center_label)

        timestamp = datetime.datetime.now().isoformat()
        stdscr.addstr(self.height - 1, self.width - 2 - len(timestamp), timestamp)

        stdscr.refresh()


def constrain(value: int, one_past_maximum: int) -> int:
    if wraparound:
        return value % one_past_maximum

    return min(max(value, 0), one_past_maximum - 1)


def neighboring_indices(
    max_width: int, max_height: int, row_index: int, column_index: int
) -> set[tuple[int, int]]:
    unconstrained_indices = (
        (p[0] + row_index, p[1] + column_index)
        for p in itertools.product(range(-1, 2), repeat=2)
        if p != (0, 0)
    )

    constrained_indices = set()

    for candidate_row, candidate_column in unconstrained_indices:
        candidate_row = constrain(candidate_row, max_height)
        candidate_column = constrain(candidate_column, max_width)

        constrained_indices.add((candidate_row, candidate_column))

    return constrained_indices


def main(stdscr: curses.window) -> None:
    board = Board(curses.tigetnum("lines"), curses.tigetnum("cols"))

    board.add_seed(life_seeds.r_pentomino)

    try:
        stdscr.clear()
        start_time = time.time()
        for generation in itertools.count():
            gps = generation / (time.time() - start_time)
            board.draw(stdscr, f"{gps:.2f} generations per second")
            board.update()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    curses.wrapper(main)
