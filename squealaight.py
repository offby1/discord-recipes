import sqlite3


conn = sqlite3.connect('example.db')
conn.row_factory = sqlite3.Row

cursor = conn.cursor()

with conn:
    conn.execute('''CREATE TABLE IF NOT EXISTS books
    (ISBN text, BookName text, BookAuthor text, BookTaken bool,
    CONSTRAINT pk PRIMARY KEY (BookName, BookAuthor)
    )''')

    conn.execute("""INSERT OR REPLACE INTO books
    (ISBN, BookName, BookAuthor, BookTaken)
    values("123-456-7890", "A test of ISBN", "ISBN working group", False)""")

    conn.execute("""INSERT OR REPLACE INTO books
    values("456-789-012", "A second test of ISBN", "ISBN working group", False)""")

cursor.execute("SELECT rowid, ISBN, BookName, BookAuthor, BookTaken from books")


def books_by_author(cursor, author):
    cursor.execute("SELECT BookAuthor from books WHERE BookAuthor = ? LIMIT 1", (author,))
    return cursor.fetchone()


print(books_by_author(cursor, "ISBN working group")["BookAuthor"])
