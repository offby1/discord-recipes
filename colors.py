import enum
import random

class Colors(enum.Enum):

    black     = (0,0,0)
    blue      = (0,0,255)
    green     = (0,128,0)
    lime      = (0,255,0)
    magenta   = (255,0,255)
    maroon    = (128,0,0)
    navy      = (0,0,128)
    olive     = (128,128,0)
    orange    = (255,165,0)
    purple    = (128,0,128)
    red       = (255,0,0)
    silver    = (192,192,192)
    teal      = (0,128,128)
    turquosie = (0, 255,255)
    white     = (255, 255, 255)
    yellow    = (255,215,0)

random_tuple = tuple(random.choices(range(0, 255), k=3))

def tuple_distance(t1, t2):
    return sum((p[0] - p[1]) ** 2 for p in zip(t1, t2))

print(f"{random_tuple=} is closest to {min(Colors, key=lambda c: tuple_distance(c.value, random_tuple))}")
