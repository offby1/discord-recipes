import dataclasses
import functools
from typing import Any, Iterable, Union, cast

import pandas as pd
import tabulate


@functools.total_ordering
@dataclasses.dataclass(frozen=True, kw_only=True)
class Location:
    # See https://en.wikipedia.org/wiki/Chess#Notation
    File: str  # column
    Rank: int  # row

    def __post_init__(self) -> None:
        assert self.File in "abcdefgh"
        assert self.Rank in range(1, 9)

    def file_number(self) -> int:
        return ord(self.File) - ord("a") + 1

    def __lt__(self, other: Any) -> bool:
        if self.Rank < other.Rank:
            return True
        if self.Rank > other.Rank:
            return False
        return bool(self.File < other.File)


class Queen(Location):
    def attacks(self, other: Location) -> bool:
        if self.Rank == other.Rank:
            return True
        if self.File == other.File:
            return True

        if self.Rank - self.file_number() == other.Rank - other.file_number():
            return True

        if self.Rank + self.file_number() == other.Rank + other.file_number():
            return True

        return False

    @classmethod
    def from_numbers(kls, rank_number: int, file_number: int) -> "Queen":
        File = chr(file_number - 1 + ord("a"))
        return kls(Rank=rank_number, File=File)


@dataclasses.dataclass()
class Solution:
    queens: set[Queen]

    # def __init__(self, q: Queen) -> None:

    def __post_init__(self) -> None:
        assert isinstance(self.queens, set)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Solution):
            return NotImplemented
        return bool(self.queens == other.queens)

    def __hash__(self) -> int:
        return hash((q.Rank, q.File) for q in sorted(self.queens))

    def size(self) -> int:
        return len(self.queens)

    def maybe_add(self, q: Queen) -> Union["Solution", None]:
        for potential_attacker in self.queens:
            if potential_attacker.attacks(q):
                return None

        new_set = self.queens.union([q])
        return Solution(queens=new_set)

    def maybe_add_queen_at(
        self,
        *,
        rank_number: int,
        file_number: int,
    ) -> Union["Solution", None]:
        q = Queen.from_numbers(rank_number=rank_number, file_number=file_number)
        return self.maybe_add(q)

    def rightmost_empty_file_number(self) -> int:
        return 8 - self.size()

    def __repr__(self) -> str:
        return f"Solution(queens={sorted(self.queens)})"


def n_queen_solutions(num_files: int) -> set[Solution]:
    if num_files <= 1:
        return {
            Solution({Queen.from_numbers(rank_number=rank_number, file_number=8)})
            for rank_number in range(1, 9)
        }

    existing_solutions = n_queen_solutions(num_files - 1)
    new_solutions = set()
    for s in existing_solutions:
        for _candidate_rank in range(1, 9):
            if new_solution := s.maybe_add_queen_at(
                rank_number=_candidate_rank,
                file_number=s.rightmost_empty_file_number(),
            ):
                new_solutions.add(new_solution)

    assert new_solutions, existing_solutions

    return new_solutions


def test_attack() -> None:
    lower_left = Queen(Rank=1, File="a")
    assert lower_left.attacks(lower_left)
    assert lower_left.attacks(Location(Rank=2, File="a"))
    assert lower_left.attacks(Location(Rank=1, File="b"))
    assert lower_left.attacks(Location(Rank=2, File="b"))

    assert not lower_left.attacks(Location(Rank=3, File="b"))
    assert not lower_left.attacks(Location(Rank=2, File="c"))


def test_another() -> None:
    d_one = Queen(Rank=1, File="d")
    assert d_one.attacks(Location(Rank=4, File="a"))

    g_one = Queen(Rank=1, File="g")
    f_two = Queen(Rank=2, File="f")
    assert g_one.attacks(f_two)
    assert f_two.attacks(g_one)


def test_solution_sanity() -> None:
    otta_be_five = n_queen_solutions(5).pop()
    assert len(otta_be_five.queens) == 5

    otta_be_eight = n_queen_solutions(8).pop()
    assert len(otta_be_eight.queens) == 8


for index, one_solution in enumerate(n_queen_solutions(8)):
    if index > 0:
        print()
    print(f"\f\n{index}:")
    df = pd.DataFrame(index=range(1, 9), columns=range(8), data=" ")
    for queen in one_solution.queens:
        df[queen.Rank - 1][queen.file_number() - 1] = "*"

    print(
        tabulate.tabulate(
            cast(Iterable[Iterable[Any]], df),
            tablefmt="grid",
            headers="abcdefgh",
        ),
    )
