#!/usr/bin/env uv run

# /// script
# dependencies = [
#   "tabulate"
# ]
# ///
from __future__ import annotations

import collections
import contextlib
import dataclasses
import multiprocessing
from typing import TYPE_CHECKING, Any

import tabulate

import wordle_words

if TYPE_CHECKING:
    from collections.abc import Iterable

ABSENT = "1"
PRESENT = "2"
CORRECT = "3"


@dataclasses.dataclass
class Filter:
    minima_by_letter: dict[str, int]
    maxima_by_letter: dict[str, int]
    facts_by_slot: list[str]

    @classmethod
    def from_colors(cls, guess: str, digits: str) -> Filter:
        minima_by_letter: dict[str, int] = collections.defaultdict(int)
        maxima_by_letter: dict[str, int] = collections.defaultdict(int)
        facts_by_slot: list[Any] = [None] * len(guess)

        for status in [CORRECT, PRESENT, ABSENT]:
            for slot, letter in enumerate(guess):
                if status == digits[slot]:
                    if status == CORRECT:
                        minima_by_letter[letter] += 1
                        facts_by_slot[slot] = letter
                    elif status == PRESENT:
                        minima_by_letter[letter] += 1
                        facts_by_slot[slot] = f"!{letter}"
                    else:
                        maxima_by_letter[letter] = minima_by_letter[letter]
                        facts_by_slot[slot] = f"!{letter}"

        return cls(minima_by_letter, maxima_by_letter, facts_by_slot)

    def __call__(self, word: str) -> bool:
        this_word_histogram = collections.Counter(word)

        for letter, c in self.minima_by_letter.items():
            if this_word_histogram[letter] < c:
                return False

        for letter, c in self.maxima_by_letter.items():
            if this_word_histogram[letter] > c:
                return False

        for letter, fact in zip(word, self.facts_by_slot):
            if fact[0] == "!":
                if fact[1] == letter:
                    return False
            elif fact[0] != letter:
                return False

        return True


def yield_possibilities(
    guess: str,
    colors: int,
    possible_words: Iterable[str],
    other_words: Iterable[str],
) -> tuple[list[str], list[str]]:
    filter_ = Filter.from_colors(guess, str(colors))

    return (
        [w for w in possible_words if filter_(w)],
        [w for w in other_words if filter_(w)],
    )


def wordle_digits_from_guess(*, secret_word: str, guess: str) -> int:
    secret_word_list = list(secret_word)
    guess_letters = list(guess)
    result: list[Any] = [0] * len(guess_letters)

    for slots_examined in range(len(secret_word_list)):
        if secret_word_list[slots_examined] == guess_letters[slots_examined]:
            result[slots_examined] = CORRECT
            secret_word_list[slots_examined] = "_"

    for slots_examined in range(len(secret_word_list)):
        if result[slots_examined] != 0:
            continue

        g = guess_letters[slots_examined]

        guess_index = None
        with contextlib.suppress(ValueError):
            guess_index = secret_word_list.index(g)

        if guess_index is not None:
            result[slots_examined] = PRESENT
            secret_word_list[guess_index] = "_"

        else:
            result[slots_examined] = ABSENT

    return int("".join([str(d) for d in result]))


def test_wdfg() -> None:
    assert wordle_digits_from_guess(guess="meter", secret_word="inter") == 11333
    assert wordle_digits_from_guess(guess="meter", secret_word="enter") == 12333


def group_em(guess: str, wordlist: list[str]) -> collections.defaultdict[int, set[str]]:
    groups = collections.defaultdict(set)

    annotated_wordlist = [
        (w, wordle_digits_from_guess(secret_word=w, guess=guess)) for w in wordlist
    ]

    for potential_secret, result in annotated_wordlist:
        groups[result].add(potential_secret)

    return groups


def score(guess: str, wordlist: list[str]) -> tuple[tuple[int, int], str]:
    groups = group_em(guess, wordlist)
    if not groups:
        return ((0, 0), guess)
    size_of_largest_group = max([len(v) for v in groups.values()])
    return (len(groups), -size_of_largest_group), guess


def best_guesses(
    possible_words: list[str],
    other_words: list[str],
) -> list[tuple[int, str, str]]:
    with multiprocessing.Pool() as pool:
        scores = pool.starmap(
            score,
            [(w, possible_words) for w in possible_words + other_words],
        )

        # turn the numeric tuple into a percentile, like how wordlebot does it
        rejiggered = []
        for index, (_, word) in enumerate(sorted(scores, reverse=True)):
            num_smaller = len(scores) - index - 1
            millile = num_smaller / len(scores)
            millile = 1000 * millile
            millile = int(round(millile, 3))

            hint = ""
            if word in wordle_words.possible_answers:
                hint = "*"

            new_datum = (millile, word, hint)

            rejiggered.append(new_datum)

            if index == 39:
                break

        return rejiggered


if __name__ == "__main__":
    guess = None

    possible_answers = wordle_words.possible_answers
    other_words = wordle_words.other_words

    # Which first word?
    # * For a long time I used "least"
    # * as of 1 July 2023 it looks like the wordle bot starts with "slate"
    # * running this with an empty list gives many words ranked at 99, of which "tares" is the first, and "parse" is the first with an asterisk
    for guess, result in [("parse", 12123), ("stale", 31313)]:
        possible_answers, other_words = yield_possibilities(
            guess,
            result,
            possible_answers,
            other_words,
        )

    print(
        tabulate.tabulate(
            best_guesses(possible_answers, other_words),
            headers=("score", "word", "hint"),
        ),
    )
