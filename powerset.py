def powerset(items):
    result = set()

    if len(items) <= 1:
        return set([frozenset(items)]).union(set([frozenset([])]))

    frc = powerset(items[1:])
    for frznst in frc:
        result.add(frznst)
        result.add(frznst.union(items[0:1]))

    return result


def test_some():
    assert len(powerset(range(10))) == pow(2, 10)

    assert powerset(range(3)) == {
        frozenset(),
        frozenset({0, 1, 2}),
        frozenset({0, 1}),
        frozenset({0, 2}),
        frozenset({0}),
        frozenset({1, 2}),
        frozenset({1}),
        frozenset({2}),
    }
