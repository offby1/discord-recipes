class HashTable:
    def __init__(self):
        self.buckets = [[] for _ in range(17)]

    def _size(self):
        return sum([len(x) for x in self.buckets])

    def __repr__(self):
        return f"{len(self.buckets)=} {self._size()=}"

    def _hash_bucket_for_item(self, item):
        index = hash(item) % len(self.buckets)
        rv = self.buckets[index]
        return rv

    def add(self, key, value):
        bucket = self._hash_bucket_for_item(key)
        for k_v_pair in bucket:
            if k_v_pair[0] == key:
                k_v_pair[1] = value
                break
        else:
            bucket.append([key, value])

    def get(self, key):
        bucket = self._hash_bucket_for_item(key)
        for k, v in bucket:
            if k == key:
                return v
        raise KeyError(f"Ain't found {key}")


if __name__ == "__main__":
    h = HashTable()

    for x in range(2):
        for i in range(20):
            h.add(i, f"{i=} {x=}")

    import pprint

    pprint.pprint([h.get(i) for i in range(20)])
    print(h)
