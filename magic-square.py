# test data is from  https://en.wikipedia.org/wiki/Magic_square

non_magic = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

magics = [
    [[2, 7, 6], [9, 5, 1], [4, 3, 8]],
    [[2, 3, 5, 8], [5, 8, 2, 3], [4, 1, 7, 6], [7, 6, 4, 1]],
    [[10, 3, 13, 8], [5, 16, 2, 11], [4, 9, 7, 14], [15, 6, 12, 1]],
]


def columns(sq):
    return list(zip(*sq))


def diagonals(sq):
    yield [r[i] for i, r in enumerate(sq)]
    yield [r[-(i + 1)] for i, r in enumerate(sq)]


def is_magic(sq):
    row_sums = [sum(r) for r in sq]
    column_sums = [sum(c) for c in columns(sq)]
    diagonal_sums = [sum(d) for d in diagonals(sq)]

    return set(row_sums) == set(column_sums) == set(diagonal_sums)


assert not (is_magic(non_magic))

for m in magics:
    assert is_magic(m)
