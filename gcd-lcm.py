import functools


def euclid_gcd(a, b):
    if a < b:
        a, b = b, a

    if a == b:
        return a

    return euclid_gcd(a - b, b)


def generalized_gcd(*args):
    m = max(args)
    return functools.reduce(euclid_gcd, args, m)


def lcm(a, b):
    return abs(a * b) // euclid_gcd(a, b)


def generalized_lcm(*args):
    m = min(args)
    return functools.reduce(lcm, args, m)


print(euclid_gcd(10, 11))
print(euclid_gcd(10, 12))
print(euclid_gcd(10, 20))
print(euclid_gcd(7, 20))
print(euclid_gcd(7, 21))


print(functools.reduce(lcm, range(2, 22, 2), 1))
