def cube_root_by_newtons_method(n: float) -> float:
    def x_cubed_minus_n(x: float) -> float:
        return x * x * x - n

    def first_derivative(x: float) -> float:
        return 2 * x * x

    guess = 1.0

    while True:
        print(f"{guess=}")
        updated_guess = guess - x_cubed_minus_n(guess) / first_derivative(guess)
        error = abs(updated_guess - guess) / guess
        guess = updated_guess

        if error < 0.01:
            return guess


print(cube_root_by_newtons_method(1000))
print(cube_root_by_newtons_method(8))
