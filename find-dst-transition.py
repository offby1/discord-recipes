# This is easy if you use pytz -- https://stackoverflow.com/a/7373550
# But I want to make it hard on myself :)
from __future__ import annotations

from datetime import datetime, timedelta, timezone, tzinfo
from typing import Iterable

from zoneinfo import ZoneInfo


def round_dt(dt: datetime) -> datetime:
    second = dt.second
    second = int(round(second))
    return dt.replace(second=second, microsecond=0)


def dst_flavor(zone: tzinfo, some_date: datetime) -> bool:
    rv = some_date.astimezone(zone).dst()
    return bool(rv)


def _find_by_binary_search(
    zone: tzinfo,
    one_kind: datetime,
    other_kind: datetime,
) -> tuple[datetime, datetime]:
    lowest = one_kind
    highest = other_kind

    assert dst_flavor(zone, highest) != dst_flavor(
        zone,
        lowest,
    ), f"{lowest=} and {highest=} have the same DST mojo in {zone=}"

    while dst_flavor(zone, highest) != dst_flavor(zone, lowest):
        candidate = lowest + (highest - lowest) / 2
        if dst_flavor(zone, candidate) == dst_flavor(zone, lowest):
            lowest = candidate
        else:
            highest = candidate

        if highest - lowest < timedelta(seconds=0.1):
            return round_dt(lowest.astimezone(zone)), round_dt(highest.astimezone(zone))

    raise AssertionError


def find_dst_transitions(zone: tzinfo) -> Iterable[tuple[datetime, datetime]]:
    now = datetime.now(tz=timezone.utc)

    # Walk forward three months at a time until we've found a date in the future whose dst is different from now, and
    # another whose dst is different from *that*.

    dates_with_alternating_flavors = [now]

    candidate = now
    while len(dates_with_alternating_flavors) < 3:
        if dst_flavor(zone, candidate) != dst_flavor(
            zone,
            dates_with_alternating_flavors[-1],
        ):
            dates_with_alternating_flavors.append(candidate)
        candidate += timedelta(days=90)

        if len(dates_with_alternating_flavors) == 1 and candidate - now >= timedelta(
            days=365,
        ):
            print(
                f"We've looked ahead a whole year, and not found any dates with different flavors, so I guess {zone} doesn't do DST",
            )
            return []

    return [
        _find_by_binary_search(
            zone,
            dates_with_alternating_flavors[0],
            dates_with_alternating_flavors[1],
        ),
        _find_by_binary_search(
            zone,
            dates_with_alternating_flavors[1],
            dates_with_alternating_flavors[2],
        ),
    ]


def dst_description(aware_dt: datetime) -> str:
    tz_descr = aware_dt.tzinfo.tzname(aware_dt)
    dst_descr = "*" if aware_dt.dst() else " "
    return f"{dst_descr} ({tz_descr})"


zone_names_by_transition_dates = {}
for counter, zone in enumerate(
    [
        ZoneInfo("Australia/Brisbane"),
        ZoneInfo("Australia/Melbourne"),
        ZoneInfo("Australia/Adelaide"),
        ZoneInfo("Pacific/Fiji"),
        ZoneInfo("Europe/London"),
        ZoneInfo("America/Los_Angeles"),
        timezone(timedelta(seconds=-25200)),
        ZoneInfo("America/New_York"),
        ZoneInfo(key="Asia/Tehran"),
        ZoneInfo(key="UTC"),
    ],
):
    if counter > 0:
        print()
    print(zone)

    try:
        for before, after in find_dst_transitions(zone):
            print(
                f"{before} {dst_description(before):10} -> {after} {dst_description(after)}",
            )
            zone_names_by_transition_dates[before] = zone
    except TypeError as e:
        print(f"none ({e})")

print()
print("Transition dates in order")
print()
for when, zone in sorted(zone_names_by_transition_dates.items()):
    print(f"{when.astimezone()}: {zone}")
