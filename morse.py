morseDictionary = {
    "0": "-----",
    "1": ".----",
    "2": "..---",
    "3": "...--",
    "4": "....-",
    "5": ".....",
    "6": "-....",
    "7": "--...",
    "8": "---..",
    "9": "----.",
    "A": ".-",
    "B": "-...",
    "C": "-.-.",
    "D": "-..",
    "E": ".",
    "F": "..-.",
    "G": "--.",
    "H": "....",
    "I": "..",
    "J": ".---",
    "K": "-.-",
    "L": ".-..",
    "M": "--",
    "N": "-.",
    "O": "---",
    "P": ".--.",
    "Q": "--.-",
    "R": ".-.",
    "S": "...",
    "T": "-",
    "U": "..-",
    "V": "...-",
    "W": ".--",
    "X": "-..-",
    "Y": "-.--",
    "Z": "--..",
    ".": ".-.-.-",
    ",": "--..--",
    "?": "..--..",
    "!": "-.-.--",
    "-": "-....-",
    "/": "-..-.",
    "@": ".--.-.",
    "(": "-.--.",
    ")": "-.--.-",
}

unMorseDictionary = {v: k for k, v in morseDictionary.items()}

def morsify(chars):
    result = []
    for c in chars:
        if c == ' ':
            result.append(c)
        else:
            m = morseDictionary.get(c.upper())
            if m:
                if result and result[-1] != ' ':
                    result.append('/')
                result.append(m)

    return ''.join(result)


def unmorsify(chars):
    codes = chars.split('/')
    result = []
    for c in codes:
        if c == ' ':
            result.append(c)
        else:
            letter = unMorseDictionary.get(c)
            if letter:
                result.append(letter)
    return ''.join(result)
