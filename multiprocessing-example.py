import multiprocessing
import random
import time

import more_itertools

work_items = random.choices(range(10_000), k=10_000)


def find_factors(n):
    result = []
    for i in range(2, n):
        if n % i == 0:
            result.append(i)
    return (n, result)


def timing_test(num_processes):

    print(f"Starting timing test with {num_processes=}")

    pool = multiprocessing.Pool(processes=num_processes)

    start_time = time.time()
    results = pool.map(find_factors, work_items)
    stop_time = time.time()

    print(more_itertools.ilen(results))

    print(f"OK, with {num_processes=} it took {stop_time - start_time} seconds")


if __name__ == "__main__":
    random.seed(0)
    for i in (1, 10, 100, None):
        timing_test(i)
