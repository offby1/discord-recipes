import logging

import psycopg2

logging.basicConfig()
logger = logging.getLogger()


conn = psycopg2.connect()

query = "SELECT 3 + 3"
params = {}


def wat():
    cursor = conn.cursor()
    try:
        cursor.execute(query=query, vars=params)
        return cursor.fetchall()
    except Exception as e:
        logging.error(
            "Something went wrong retrieving data; Query: %s; Params: %s; Exception: %s"
            % (query, params, e),
        )
        return None
    finally:
        cursor.close()


print(wat())
