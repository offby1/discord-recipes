import datetime


def month(year_number, month_number):
    first_of_the_month = first_of_next_month = datetime.date(year=year_number, month=month_number, day=1)
    while first_of_next_month.month == first_of_the_month.month:
        first_of_next_month += datetime.timedelta(days=1)

    if first_of_the_month.isoweekday() == 7:
        upper_left_sunday = first_of_the_month
    else:
        upper_left_sunday = first_of_the_month - datetime.timedelta(days=first_of_the_month.isoweekday())

    weeks = []
    day = upper_left_sunday

    this_week = []

    while day < first_of_next_month:
        if day >= first_of_the_month:
            this_week.append('{:2d}'.format(day.day))
        else:
            this_week.append('  ')

        if day.isoweekday() == 6:
            weeks.append(' '.join(this_week))
            this_week = []

        day += datetime.timedelta(days=1)

    if this_week:
        weeks.append(' '.join(this_week))

    return weeks


if __name__ == "__main__":
    m = 12
    y = 2022
    print(f"Introducing ... {m} {y}")
    for week in month(y, m):
        print(week)
