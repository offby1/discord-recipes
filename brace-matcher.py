def report_mismatched_braces(s: str) -> None:
    brace_pairs = {
        "{": "}",
        "[": "]",
        "(": ")",
        "<": ">",
    }
    stack = []
    for ch in s:
        if ch in brace_pairs.keys():
            stack.append(ch)
        elif ch in brace_pairs.values():
            if not stack:
                raise ValueError(f"Closing thing {ch!r} with nothing open")
            currently_open = stack[-1]
            needed_closer = brace_pairs[currently_open]
            if ch == needed_closer:
                stack.pop()
            else:
                raise ValueError(f"{ch!r} with {currently_open} open")
    if stack:
        raise ValueError(f"{stack[-1]!r} open at end of string")


report_mismatched_braces("(())[][]![")
