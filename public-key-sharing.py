import queue
import threading
from typing import Optional

import Crypto.Util.number  # pip install PyCryptodome
from termcolor import colored  # pip install termcolor

# https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange


def red(number: int) -> str:
    return colored(str(number), "red")


def green(number: int) -> str:
    return colored(str(number), "green")


def get_prime(num_bits: int = 200) -> int:
    return Crypto.Util.number.getPrime(num_bits)


class CryptoPlayer(threading.Thread):
    def __init__(
        self, name: str, pronoun: str, public_base: int, public_modulus: int
    ) -> None:
        super().__init__()

        self.name = name
        self.pronoun = pronoun
        self.partner_name: Optional[str] = None
        self.partner_queue: Optional[queue.Queue[int]] = None
        self.public_base = public_base
        self.public_modulus = public_modulus
        self.incoming_messages: queue.Queue[int] = queue.Queue()

    def add_partner(self, partner_name: str, partner_queue: queue.Queue[int]) -> None:
        self.partner_name = partner_name
        self.partner_queue = partner_queue

    def send_to_partner(self, datum: int, description: str) -> None:
        assert self.partner_queue is not None
        self.partner_queue.put(datum)
        print(f"{self.name:<6} sends {description} to {self.partner_name}")

    def get_from_partner(self, description: str) -> int:
        message = self.incoming_messages.get()
        print(
            f"{self.name:<6} gets {description} {green(message)} from {self.partner_name}"
        )
        return message

    def run(self) -> None:
        self.generate_shared_secret()

    def generate_shared_secret(self) -> None:
        modulus_bit_length = self.public_modulus.bit_length()

        # For some reason, you can get e.g. 17 from both get_prime(5) and get_prime(4), so we gotta try a few times to
        # ensure that our secret is less than the modulus.
        while True:
            secret_prime = get_prime(modulus_bit_length - 1)

            if secret_prime < self.public_modulus:
                break

        print(
            f"{self.name:<6} chooses {red(secret_prime)} as {self.pronoun} secret key"
        )
        public_key = pow(self.public_base, secret_prime, self.public_modulus)
        print(
            f"{self.name:<6} computes {self.public_base} ^ {red(secret_prime)} = {green(public_key)}"
        )
        self.send_to_partner(public_key, f"{self.name}'s public key")

        partners_public_key = self.get_from_partner(f"{self.partner_name}'s public key")

        self.shared_secret = pow(partners_public_key, secret_prime, self.public_modulus)
        print(
            f"{self.name:<6} computes {green(partners_public_key)} ^ {red(secret_prime)} = {red(self.shared_secret)}"
        )

    def __str__(self) -> str:
        return str(self.name)


if __name__ == "__main__":
    public_modulus = get_prime()
    public_base = 2
    print(f"\n{public_modulus=}\n{public_base=}\n")
    alice = CryptoPlayer(
        name="Alice",
        pronoun="her",
        public_base=public_base,
        public_modulus=public_modulus,
    )
    bob__ = CryptoPlayer(
        name="Bob",
        pronoun="his",
        public_base=public_base,
        public_modulus=public_modulus,
    )
    alice.add_partner(bob__.name, bob__.incoming_messages)
    bob__.add_partner(alice.name, alice.incoming_messages)

    for t in (alice, bob__):
        t.start()

    for t in (alice, bob__):
        t.join()
