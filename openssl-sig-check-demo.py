# from the first set of examples at
# https://stackoverflow.com/questions/6350031/how-to-verify-in-pycrypto-signature-created-by-openssl/32896372#32896372
# tweaked for Python3, and tidied up
"""
Create the various files like this

$ openssl genrsa -out priv.pem
$ openssl rsa -pubout -in priv.pem -out pub.pem
$ echo test123 > test.txt
$ openssl dgst -sha1 -sign priv.pem -out test.txt.sig test.txt
$ openssl dgst -sha1 -verify pub.pem -signature test.txt.sig test.txt

"""

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5

with open('pub.pem', 'r') as fd:
    pub_cipher = PKCS1_v1_5.new(RSA.importKey(fd.read()))

with open('test.txt', 'rb') as fd:
    message_hash = SHA.new(fd.read())

with open('test.txt.sig', 'rb') as fd:
    signature = fd.read()

verified = pub_cipher.verify(message_hash, signature)
print(verified)

# regenerate the signature and compare, just for fun
with open('priv.pem', 'r') as fd:
    priv_cipher = PKCS1_v1_5.new(RSA.importKey(fd.read()))

signature2 = priv_cipher.sign(message_hash)

if signature == signature2:
    print("Match!! :)")
else:
    print(f"Uh oh -- supplied signature\n{signature.hex()[0:20]}... differs from signature that we just computed: {signature2.hex()[0:20]}...")
