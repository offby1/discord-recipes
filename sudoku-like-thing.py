import itertools
from pprint import pp

# all the sets of 7 digits, with no repeats, that add up to 40
pp(set(frozenset(p) for p in itertools.permutations(range(1, 10), r=7) if sum(p) == 40))

# {frozenset({2, 3, 5, 6, 7, 8, 9}), frozenset({1, 4, 5, 6, 7, 8, 9})}
