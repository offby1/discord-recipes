# https://en.wikipedia.org/wiki/Playfair_cipher#Description

"""
Demo like this
$ python3 playfair.py --decrypt --text=$(python3 playfair.py)
"""

import argparse
import string
import sys

TABLE_WIDTH = 5
TABLE_HEIGHT = 5


def _lower_case_letters_only(str_):
    return [letter for letter in str_.lower() if letter in string.ascii_lowercase]


class Table:
    def __init__(self, letters_to_coordinates, coordinates_to_letters):
        self.letters_to_coordinates = letters_to_coordinates
        self.coordinates_to_letters = coordinates_to_letters

    @classmethod
    def from_keyword(kls, keyword):
        keyword = _lower_case_letters_only(keyword)

        keyword = [
            letter for letter in keyword if letter != 'j'
        ]  # dirty hack to make a 26-letter alphebet fit into 25 slots
        sans_dups = []
        for letter in keyword:
            if letter not in sans_dups:
                sans_dups.append(letter)

        sans_dups = sans_dups[: TABLE_WIDTH * TABLE_HEIGHT]  # so that it doesn't overflow the table
        # pad it out to TABLE_WIDTH * TABLE_HEIGHT letters
        for letter in string.ascii_lowercase:
            if letter == 'j':
                continue

            if len(sans_dups) == TABLE_WIDTH * TABLE_HEIGHT:
                break
            if letter not in sans_dups:
                sans_dups.append(letter)

        letters_to_coordinates = {}
        coordinates_to_letters = {}
        for index, letter in enumerate(sans_dups):
            row, column = divmod(index, 5)
            letters_to_coordinates[letter] = row, column
            coordinates_to_letters[(row, column)] = letter
        return kls(letters_to_coordinates, coordinates_to_letters)

    def encrypt_digram(self, dg, encrypt=True):
        dg = dg.copy()

        # If the letters appear on the same row of your table, replace them with the letters to their immediate right
        # respectively (wrapping around to the left side of the row if a letter in the original pair was on the right side
        # of the row).
        coordinates = [self.letters_to_coordinates[letter] for letter in dg]

        def row_neighbor(c):
            row, column = c
            column = (column + (1 if encrypt else -1)) % TABLE_WIDTH
            return row, column

        def column_neighbor(c):
            row, column = c
            row = (row + (1 if encrypt else -1)) % TABLE_HEIGHT
            return row, column

        if coordinates[0][0] == coordinates[1][0]:
            return [self.coordinates_to_letters[row_neighbor(c)] for c in coordinates]

        # If the letters appear on the same column of your table, replace them with the letters immediately below respectively
        # (wrapping around to the top side of the column if a letter in the original pair was on the bottom side of the
        # column).
        if coordinates[0][1] == coordinates[1][1]:
            return [self.coordinates_to_letters[column_neighbor(c)] for c in coordinates]

        # If the letters are not on the same row or column, replace them with the letters on the same row respectively but at the
        # other pair of corners of the rectangle defined by the original pair. The order is important – the first letter of the
        # encrypted pair is the one that lies on the same row as the first letter of the plaintext pair.

        first_new_coords = coordinates[0][0], coordinates[1][1]
        second_new_coords = coordinates[1][0], coordinates[0][1]

        if not encrypt:
            first_new_coords, second_new_coords = second_new_coords, first_new_coords

        if encrypt:
            return [
                self.coordinates_to_letters[first_new_coords],
                self.coordinates_to_letters[second_new_coords],
            ]
        else:
            return [
                self.coordinates_to_letters[second_new_coords],
                self.coordinates_to_letters[first_new_coords],
            ]

    def dump(self):
        for row in range(5):
            for column in range(5):
                print(self.coordinates_to_letters[row, column], end='', file=sys.stderr)
            print(file=sys.stderr)
        print(file=sys.stderr)


# See also https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.chunked
def batches(iterable, size):
    for i in range(0, len(iterable), size):
        yield list(iterable[i : i + size])


def make_digrams(characters):
    # First pass: insert 'x' between duplicated letters (but only if the first of the two is in an even-numbered
    # position)
    with_xs = []
    for index, c in enumerate(characters):
        if index % 2 == 1:
            if c == with_xs[-1]:
                with_xs.append('x')
        with_xs.append(c)

    digrams = list(batches(with_xs, 2))

    while len(digrams[-1]) < 2:
        digrams[-1].append('x')

    return digrams


def encrypt(plaintext, keyword, decrypt):
    table = Table.from_keyword(keyword)

    table.dump()

    plaintext_digrams = make_digrams(_lower_case_letters_only(plaintext))

    ciphertext_digrams = []
    for d in plaintext_digrams:
        ciphertext_digrams.append(table.encrypt_digram(d, encrypt=not (decrypt)))

    return ''.join(''.join(dg) for dg in ciphertext_digrams)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Encrypt and decrypt with the Playfair Cipher (https://en.wikipedia.org/wiki/Playfair_cipher)"
    )
    parser.add_argument(
        "--text",
        type=str,
        default="Hide the gold in the tree stump",
        help="text to be encrypted or decrypted",
    )
    parser.add_argument("--keyword", type=str, default="playfair example", help="secret key")
    parser.add_argument("--decrypt", default=False, action="store_true")
    args = parser.parse_args()

    print(
        encrypt(
            plaintext=args.text,
            keyword=args.keyword,
            decrypt=args.decrypt,
        )
    )
