def _all_substrings(s):
    for start_index in range(len(s)):
        for length in range(1, 1 + len(s) - start_index):
            yield s[start_index : length + start_index]


def _is_alphabetized(s):
    return list(s) == sorted(s)


def longest_alphabetical_substring(s):
    alphabetical_substrings = (ss for ss in _all_substrings(s) if _is_alphabetized(ss))
    return max(alphabetical_substrings, key=len)
