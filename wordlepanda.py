import math

import pandas as pd

import wordle_cheat
import wordle_words


master_wordlist = wordle_words.possible_answers + wordle_words.other_words


df = pd.DataFrame(master_wordlist)[0:100]
print(f"OK this takes a minute -- merging ... 'bout {pow(10, round(math.log10(pow(len(df), 2)))):,} rows")
df = df.merge(df, how="cross")

print("Computing scores")
df["score"] = df.apply(lambda series: wordle_cheat.wordle_digits_from_guess(secret_word=series[0], guess=series[1]), "columns")
print(df[df["score"] != 33333].sort_values(by="score", ascending=False))
