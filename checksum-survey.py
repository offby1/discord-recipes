"""
Find duplicate files; display info about them
"""

import collections
import hashlib
import json
import operator
import pathlib

import tqdm

root_directories = [
    pathlib.Path(s).expanduser()
    for s in (
        "~/Google Drive/Dropbox",
        "~/Google Drive/Pix from Dropbox",
        "~/Google Drive/Rescued-From-Dropbox",
    )
]
TARGET = root_directories[-1]


def find_files_under_roots(root_directories):
    filenames_by_root = collections.defaultdict(set)

    for d in root_directories:
        for p in tqdm.tqdm(pathlib.Path(d).glob("**/*"), desc=str(d), unit="files"):
            if p.is_file():
                filenames_by_root[p].add(p)

    return filenames_by_root


def checksum(p):
    m = hashlib.md5()
    with open(p, "rb") as inf:
        m.update(inf.read())
    return m.digest().hex()


def load_cache_from(inf):
    """
    Read the JSON file, convert all the string values to pathlib.Paths
    """
    filenames_by_checksum = json.load(inf)
    print(f"Read cache from {inf.name}")

    return {
        ch: [pathlib.Path(fn) for fn in filenames]
        for ch, filenames in filenames_by_checksum.items()
    }


def save_cache_to(filenames_by_checksum, outf):
    """
    Write the JSON file, after converting all the pathlib.Paths values to strings
    """
    json.dump(
        {ch: [str(p) for p in paths] for ch, paths in filenames_by_checksum.items()},
        outf,
    )
    print(f"Wrote cache to {outf.name}")


def checksum_files_under_roots(root_directories):
    cache_filename = pathlib.Path("/tmp/checksum_files_under_roots.json")
    try:
        with open(cache_filename) as inf:
            return load_cache_from(inf)
    except Exception as e:
        print(f"No {cache_filename} ({e}); doin' all the work")

    with open(cache_filename, "w") as outf:
        filenames_by_root = find_files_under_roots(root_directories)
        filenames_by_checksum = collections.defaultdict(list)

        for fn in tqdm.tqdm(filenames_by_root, unit="files"):

            filenames_by_checksum[checksum(fn)].append(fn)

            # if len(filenames_by_checksum[checksum(fn)]) > 1:
            #     # Found our first dupe!
            #     break

        try:
            save_cache_to(filenames_by_checksum, outf)
        except Exception:
            print(f"wtf is wrong with {filenames_by_checksum}")
            raise
        return filenames_by_checksum


filenames_by_checksum = checksum_files_under_roots(root_directories)

print(
    f"found {len(filenames_by_checksum)} unique checksums out of {sum(len(fs) for fs in filenames_by_checksum.values())} files"
)
print(
    f"That's {sum(len(fs) for fs in filenames_by_checksum.values()) - len(filenames_by_checksum)} checksums with at least one duplicate"
)

pathnames_by_number_of_duplicates = collections.defaultdict(list)

for cksum, files in filenames_by_checksum.items():
    if len(files) > 1:
        pathnames_by_number_of_duplicates[len(files)].extend(files)


def root(f):
    for r in root_directories:
        if r in f.parents:
            return r


files_by_root = collections.defaultdict(list)
for num_dups, files in sorted(
    pathnames_by_number_of_duplicates.items(), key=operator.itemgetter(0), reverse=True
):
    for index, f in enumerate(sorted(files)):
        files_by_root[root(f)].append(f)

print("PROPOSAL")
for root, files in files_by_root.items():
    if root == TARGET:
        continue
    for f in files:
        f = str(f)
        print(f"mv {f!r} {TARGET!r}")

for chksum, filenames in sorted(filenames_by_checksum.items()):
    if len(filenames) > 1:

        def num_components(path):
            return len(path.parts)

        longest_last = sorted(filenames, key=num_components)
        last_man_standing = longest_last.pop()
        for f in longest_last:
            try:
                pass
                # f.unlink()
            except FileNotFoundError:
                print(f"{f!r} is already gone")
            else:
                print(f"removed {f!r} # (copy of {last_man_standing!r})")
