Python libraries that everyone always talks about.

# Numpy
https://github.com/numpy/numpy
"Numerical Computing".
"NumPy’s main object is the homogeneous multidimensional array."
A lot is written in C.

# Pandas
https://github.com/pandas-dev/pandas
Higher-level than NumPy; built upon it.

# SciPy
https://github.com/scipy/scipy
"statistics, optimization, integration, linear algebra ..."

Fair amount of Fortran, C, C++

# Matplotlib
# pyspark
# ipython
# jupyter
