def triangle(width, increasing=True):
    r = range(1, width + 1) if increasing else range(width, 0, -1)
    for w in r:
        yield "*" * w


def half_diamond(width):
    yield from triangle(width, increasing=True)
    yield from triangle(width, increasing=False)


def full_diamond(width):
    for row in half_diamond(width):
        padding = " " * (width - len(row))
        yield padding + row + row


print("\n".join(full_diamond(10)))
