# the standard library has "textwrap"; it's unlikely the below is any better than that

def wrap_to_column(c, text):
    result_lines = []
    for ch in text:
        if not result_lines:
            result_lines.append([])
        elif ch.isspace() and len(result_lines[-1]) > c:
            result_lines.append([])

        if not len(result_lines[-1]) and ch.isspace():
            continue

        result_lines[-1].append(ch)
    return '\n'.join([''.join(l) for l in result_lines])

print(wrap_to_column(10, "Heeeey everyone!  Let's get some action"))
# Heeeey everyone!
# Let's get some
# action
# >>>
