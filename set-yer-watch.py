import datetime

import pytest  # python3 -m pip install pytest
from freezegun import freeze_time  # python3 -m pip install freezegun


def should_I_set_it_today():
    """
    For the typical mechanical watch that displays the date via a wheel with 31 numbers around the circumference
    """
    today = datetime.date.today()

    if today.day != 1:
        print(f"{today=} is not the 1st of the month.")
        return False

    yesterday = today - datetime.timedelta(days=1)

    if yesterday.day == 31:
        print(f"{today=}; {yesterday=} was the 31st of the month.")
        return False

    if yesterday.month == today.month:
        print(f"{today=} and {yesterday=} are the same month.")
        return False

    print(f"OK, {today=} and {yesterday=} was NOT the 31st of the month.")
    return True


@pytest.mark.parametrize(
    "date,expected_value",
    [
        ("2020-01-01", False),
        ("2020-02-01", False),
        ("2020-03-01", True),
        ("2020-04-01", False),
        ("2020-05-01", True),
        ("2020-06-01", False),
        ("2020-07-01", True),
        ("2020-08-01", False),
        ("2020-09-01", False),
        ("2020-10-01", True),
        ("2020-11-01", False),
        ("2020-12-01", True),
    ],
)
def test_it(date, expected_value):
    with freeze_time(date):
        assert should_I_set_it_today() is expected_value


if __name__ == "__main__":
    print(f"Should you set your watch's date forward today? {should_I_set_it_today()}")
