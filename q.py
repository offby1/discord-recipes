import queue
import time
from dataclasses import dataclass


@dataclass
class WorkItem:
    name: str
    breed: str


q = queue.Queue()


def put_with_timestamp(item):
    item.enque_time = time.time()

    q.put(item)


put_with_timestamp(WorkItem("Fluffy", "Siamese"))
put_with_timestamp(WorkItem("Rover", "Mastiff"))

delays = []

while not (q.empty()):
    workitem = q.get()
    now = time.time()
    delays.append(now - workitem.enque_time)
    print(f"Doing some work on {workitem=}")

print(delays)
# Doing some work on workitem=WorkItem(name='Fluffy', breed='Siamese')
# Doing some work on workitem=WorkItem(name='Rover', breed='Mastiff')
# [3.600120544433594e-05, 7.200241088867188e-05]
