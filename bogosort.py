import random


def already_sorted(seq):
    if len(seq) < 2:
        return True

    while len(seq) > 1:
        if seq[0] > seq[1]:
            return False
        seq = seq[1:]

    return True


def bogosort(data_to_be_sorted):
    data_to_be_sorted = data_to_be_sorted.copy()

    while True:
        i1, i2 = random.sample(range(len(data_to_be_sorted)), k=2)
        elt_a, elt_b = data_to_be_sorted[i1], data_to_be_sorted[i2]
        if elt_a > elt_b:
            data_to_be_sorted[i1], data_to_be_sorted[i2] = data_to_be_sorted[i2], data_to_be_sorted[i1]

        if already_sorted(data_to_be_sorted):
            break

    return (data_to_be_sorted)


if __name__ == "__main__":
    size = 9 # any bigger and it's too slow!
    input_data = random.choices(range(size * 100), k=size)
    print(f"Before: {input_data}")
    print(f"After:  {bogosort(input_data)}")
