import os
import subprocess

import beautifultable  # python3 -m pip install beautifultable

BT = beautifultable.BeautifulTable(maxwidth=120)

BT.header = (
    "mode",
    "links",
    "owner",
    "group",
    "size",
    "last-modified date",
    "name",
)
ls_process = subprocess.run(
    ["gls", "-l", "--time-style=+%FT%T%z"],
    stdout=subprocess.PIPE,
    env=dict(os.environ, TZ="UTC"),
    check=False,
)
for line in ls_process.stdout.splitlines():
    try:
        mode, links, owner, group, size, last_modified_date, name = line.split()
    except ValueError:
        continue
    mode = mode.decode("utf-8")
    links = int(links)
    owner = owner.decode("utf-8")
    group = group.decode("utf-8")
    size = int(size)
    last_modified_date = last_modified_date.decode("utf-8")
    name = name.decode("utf-8")
    BT.append_row([mode, links, owner, group, size, last_modified_date, name])

print(BT)
