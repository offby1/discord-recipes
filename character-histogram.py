import collections
import pathlib

try:
    __here__ = pathlib.Path(__file__).resolve().parent
except NameError:
    __here__ = pathlib.Path('.').resolve()

python_files = (f for f in (__here__.iterdir()) if f.is_file() and f.name.endswith('.py'))

counts_by_character = collections.Counter()

for f in python_files:
    with open(f) as inf:
        counts_by_character.update(inf.read())

screen_width = 100              # characters
scale_factor = screen_width / counts_by_character.most_common(n=1)[0][1]
print()
for ch, count in counts_by_character.most_common():
    hashes = '#' * round(count * scale_factor)
    if hashes:
        print(f"{ch!r}: {hashes}")
