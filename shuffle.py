import random

# Simulate a [riffle shuffle](https://en.wikipedia.org/wiki/Shuffling#Riffle)


def bit_more_than_one():
    return random.choices(range(0, 5), weights=[1, 5, 5, 2, 1])[0]


def split(seq):
    len_ = len(seq) // 2
    return seq[0:len_], seq[len_:]


def merge_randomly(s1, s2):
    result = []
    while True:
        for seq in (s1, s2):
            if seq:
                take = min(bit_more_than_one(), len(seq))
                result.extend(seq[0:take])
                seq[:] = seq[take:]

        if not s1:
            result.extend(s2)
            break

        if not s2:
            result.extend(s1)
            break

    return result


seq = list(range(52))

for _ in range(8):
    seq = merge_randomly(*split(seq))

print(seq)
