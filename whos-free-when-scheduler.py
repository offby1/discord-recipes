import dataclasses
import datetime
import functools
import random
import unicodedata

import names  # pip install names
import tabulate  # pip install tabulate

free_times_by_owner = {}
for _ in range(10):
    person = names.get_first_name()

    free_times_by_owner[person] = []

    start = datetime.datetime(2022, 6, 2, 5, 9)
    while len(free_times_by_owner[person]) < 10:
        duration = datetime.timedelta(seconds=random.randint(10, 30) * 60)
        free_times_by_owner[person].append((start, start + duration))

        start += duration + datetime.timedelta(seconds=random.randint(10, 30) * 60)


@functools.total_ordering
@dataclasses.dataclass
class Timespan:
    start: datetime.datetime
    stop: datetime.datetime
    whos_free: set[str]

    @property
    def duration(self):
        return self.stop - self.start

    @property
    def fields(self):
        return list(dataclasses.asdict(self).values()) + [self.duration]

    def __str__(self):
        return f"{self.whos_free} is/are free for {self.duration} from {self.start} to {self.stop}"

    def __lt__(self, other):
        if self.start < other.start:
            return True
        elif self.start > other.start:
            return False
        return self.stop < other.stop


flat_free_times = []
for owner, free_time_tuples in free_times_by_owner.items():
    free_time_tuples = sorted(free_time_tuples)
    for ftt in free_time_tuples:
        flat_free_times.append(Timespan(*ftt, whos_free={owner}))

flat_free_times = list(sorted(flat_free_times))
print("Free times (sorted by start time)")
print(
    tabulate.tabulate(
        [e.fields for e in flat_free_times],
        headers=["start", "stop", "who", "duration"],
    )
)
print()


@functools.total_ordering
@dataclasses.dataclass
class Transition:
    owner: str
    when: datetime.datetime
    is_start: bool

    def __lt__(self, other):
        return self.when < other.when


transitions = []
for ft in flat_free_times:
    for who in ft.whos_free:
        transitions.append(Transition(owner=who, when=ft.start, is_start=True))
        transitions.append(Transition(owner=who, when=ft.stop, is_start=False))
transitions = list(sorted(transitions))
print("Events")
print(
    tabulate.tabulate(
        [
            (t.owner, t.when, "becomes free" if t.is_start else "becomes busy")
            for t in transitions
        ],
        headers=["who", "when", "what"],
    )
)
print()

who_is_free_when = []

currently_free = set()
for index, tr in enumerate(transitions):
    if tr.is_start:
        currently_free.add(tr.owner)
    else:
        if tr.owner in currently_free:
            currently_free.remove(tr.owner)

    datum = []

    datum.append(tr.when)

    datum.append(
        transitions[index + 1].when - tr.when
        if index < len(transitions) - 1
        else unicodedata.lookup("INFINITY")
    )
    datum.append(sorted(currently_free))

    who_is_free_when.append(datum)
print("Who Is Free When")
print(tabulate.tabulate(who_is_free_when, headers=["when", "Duration", "who's free"]))
print()


def find_free_time(duration_minutes, num_requred_attendees):
    for t in who_is_free_when:
        if (
            isinstance(t[1], datetime.timedelta)
            and t[1].total_seconds() >= duration_minutes * 60
        ):
            if len(t[2]) >= num_requred_attendees:
                print(f"Hey at {t[0]} {sorted(t[2])} are free for {t[1]}")


find_free_time(5, len(free_times_by_owner))
