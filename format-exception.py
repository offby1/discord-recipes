import traceback

e = None

try:
    1 / 0
except Exception as ex:
    e = ex

print("Hi world, we're not in an exception handler or anything")

for line in traceback.format_exception(type(e), e, e.__traceback__):
    print("******* ===> ", line.rstrip())
