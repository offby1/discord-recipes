def selection_sort(seq):
    sorted = []
    while seq:
        s = min(seq)
        sorted.append(s)
        seq.remove(s)
    return sorted
