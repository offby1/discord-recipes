def underscores(secret_word, guessed_letters):
    wat = []
    for c in secret_word:
        if c in guessed_letters:
            wat.append(c)
        else:
            wat.append('_')
    return ''.join(wat)


def one_round(secret_word, guessed_letters):
    print(underscores(secret_word, guessed_letters), end='  ')
    guess = input("OK, smart guy; what's your guess?")
    guessed_letters.update(list(guess))
    success = any(letter in secret_word for letter in guess)
    return success


def game(secret_word):
    guessed_letters = set()
    failures_remaining = 5
    while failures_remaining > 0:
        print(f"You have {failures_remaining=}  ", end='')
        if not one_round(secret_word, guessed_letters):
            failures_remaining -= 1
        else:
            if set(list(secret_word)).issubset(guessed_letters):
                print("Hoorah you won")
                break

    if failures_remaining == 0:
        print("You suck")


if __name__ == "__main__":
    game("weather")
