import dataclasses
import functools
import pprint
import random
from typing import Set


@dataclasses.dataclass
class Node:
    datum: int


@functools.total_ordering
class Edge:
    def __init__(self, a: Node, b: Node):
        self.stuff = tuple(sorted([a, b]))

    def neighbor(self, exclude: Node):
        assert exclude in self.stuff
        nodes = list(self.stuff)
        nodes.remove(exclude)
        return nodes.pop()

    def __hash__(self):
        return hash(tuple(self.stuff))

    def __lt__(self, other):
        return self.stuff < other.stuff

    def __iter__(self):
        return iter(self.stuff)

    def __repr__(self):
        return repr(self.stuff)


class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = set()

    def neighbors(self, n):
        assert n in self.nodes
        result = set()
        for e in self.edges:
            if n in e:
                result.add(e.neighbor(n))
        assert n not in result
        return result

    def add_nodes_randomly(self, nodes):
        for n in nodes:

            number_of_edges_to_add = 0
            if self.nodes:
                # pick 1 or more existing nodes at random
                # add edges between that those and this one

                # it might be nice to favor smaller or larger numbers here, to make a sparser or denser graph
                number_of_edges_to_add = random.randint(1, min(2, len(self.nodes)))

            nodes_to_link_to = random.sample(sorted(self.nodes), number_of_edges_to_add)

            self.nodes.add(n)
            for other in nodes_to_link_to:
                self.edges.add(Edge(n, other))

    def __repr__(self):
        return f"""
        Edges: {pprint.pformat(sorted(self.edges))}
        """


# Stolen from Wikipedia
class DepthFirstSearcher:
    def __init__(self, graph: Graph):
        self.graph = graph
        self.discovered_vertices: Set[Node] = set()

    def __call__(self, starting_vertex):
        self.discovered_vertices.add(starting_vertex)
        yield starting_vertex
        for w in self.graph.neighbors(starting_vertex):
            if w not in self.discovered_vertices:
                yield from self(w)


if __name__ == "__main__":
    random.seed(0)
    g = Graph()

    g.add_nodes_randomly(range(10))
    print(g)

    searcher = DepthFirstSearcher(g)
    random_starting_node = random.choice(list(g.nodes))
    print(list(searcher(random_starting_node)))
