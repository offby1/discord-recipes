from hypothesis import given, strategies as st


def num_to_base(n, base=2):
    digits = []
    while n:
        q, r = divmod(n, base)
        digits.insert(0, r)
        n = q
    return digits


def string_to_integer(s, base=10):
    total = 0
    for ch in s:
        try:
            value = int(ch)
        except ValueError:
            value = ord(ch.lower()) - ord("a") + 10  # 'a' => 10
        total *= base
        total += value
    return total


@given(st.integers(min_value=1), st.integers(min_value=2, max_value=36))
def test_em_all(i, base):
    s = ''.join(
        [str(d) for d in num_to_base(i, base=base)]
    )
    assert string_to_integer(s, base=base) == int(s, base=base)
