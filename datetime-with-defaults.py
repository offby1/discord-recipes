import datetime


class DateTimeWithDefaults(datetime.datetime):
    def __new__(kls, /, **kwargs):
        now = datetime.datetime.now()
        year = kwargs.get("year", now.year)
        month = kwargs.get("month", now.month)
        day = kwargs.get("day", now.day)

        print(f"{kwargs=} {year=} {month=} {day=}")
        return datetime.datetime.__new__(kls, year, month, day, **kwargs)


print(DateTimeWithDefaults(hour=3, second=2))
