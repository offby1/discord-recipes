# from https://en.wikipedia.org/wiki/Binary_search#Procedure
def binary_search(sequence, sought):
    n = len(sequence)
    L = 0
    R = n - 1
    while L <= R:
        m = (L + R) // 2
        if sequence[m] < sought:
            L = m + 1
        elif sequence[m] > sought:
            R = m - 1
        else:
            return (m, sequence[m])
    return (m, None)


if __name__ == "__main__":
    sequence = [0, 1, 3, 4]
    for sought in [*list(range(-1, 6)), 20]:
        print()
        print(f"Gonna search for {sought}")
        index, found = binary_search(sequence, sought)
        if found is None:
            print(
                f"Didn't find {sought} but you could insert it ... somewhere around {index=} ({sequence[index]=})"
            )
        else:
            print(f"Found {found} at {index}")
