import random

adjectives = [
    "angry",
    "bilious",
    "credulous",
    "doubtful",
    "erudite",
    "ferocious",
    "golden",
    "haughty",
    "indecent",
    "juvenile",
    "knowledgable",
    "lazy",
    "mendacious",
    "new",
    "original",
    "perilous",
    "queasy",
    "rapid",
    "slippery",
    "trustworthy",
    "unhinged",
    "verified",
    "wobbly",
    "xenophobic",
    "young",
    "zealous",
]

nouns = [
    "atrium",
    "Bunsen burner",
    "clavicord",
    "d-major blues harp",
    "engine",
    "fort",
    "grapefruit",
    "headwaiter",
    "ironworker",
    "joke",
    "lariat",
    "manhole cover",
    "nastygram",
    "orange",
    "pudding",
    "qualifier",
    "robot",
    "Stetson hat",
    "thruway",
    "umbrella",
    "van",
    "wax paper roll",
    "yellowfin tuna",
    "zebra",  # of course
]


def n():
    return random.choice(nouns)


def adj():
    return random.choice(adjectives)


print(
    f""" Go to the {n()} store and get me some {adj()} {n()}s """,
)
print(f""" Before the {n()}s settled the New World, {n()}s used many {adj()} {n()}s. """)
