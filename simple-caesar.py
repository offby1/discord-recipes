import string

alphabet = string.ascii_letters


def encrypt_ch(character: str, decrypt: bool = False) -> str:
    if character not in alphabet:
        return character

    offset = len(alphabet) - 3 if decrypt else 3

    i = alphabet.index(character)
    i = (i + offset) % len(alphabet)
    return alphabet[i]


def encrypt_str(s: str, decrypt: bool = False) -> str:
    return "".join(encrypt_ch(ch, decrypt=decrypt) for ch in s)


plaintext = "Attack at dawn!"
ciphertext = encrypt_str(plaintext)
roundtripped = encrypt_str(ciphertext, decrypt=True)

print(f"{plaintext=}")
print(f"{ciphertext=}")
print(f"{roundtripped=}")
