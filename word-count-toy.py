# probably some homework assignment:

# you want to
# - find the alphabetically-first 15 words from a text
# - count the number of times each of those occurs in the text
# - insert them into a priority queue based on those numbers

import collections
import queue
import re

import requests


def get_some_text():
    url = "http://all-you-can-hold-for-five-bucks.s3-website-us-west-2.amazonaws.com/"
    words_kinda = requests.get(url).text.split()
    for w in words_kinda:
        letters_only = re.sub(r"[^A-Za-z]+", "", w)

        if letters_only:
            yield letters_only.lower()


some_text = list(get_some_text())
all_counts_by_word = collections.Counter(some_text)
top_fifteen_counts_by_word = all_counts_by_word.most_common(15)

pq = queue.PriorityQueue()
for word, count in top_fifteen_counts_by_word:
    pq.put((count, word))

while not (pq.empty()):
    print(pq.get())
