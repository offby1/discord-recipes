# https://macs4200.org/chapters/04/6/affine-cipher

import dataclasses
import string

import mod  # pip install mod
import tabulate  # pip install tabulate


@dataclasses.dataclass
class System:
    modulus: int
    a: int
    m: int
    m_1: dataclasses.InitVar[int | None] = None

    def __post_init__(self, m_1):
        self.m_1 = mod.Mod(self.m, self.modulus).inverse

    def encrypt(self, x):
        x = mod.Mod(x, self.modulus)
        return self.m * x + self.a

    def decrypt(self, y):
        if y == -1:
            return y
        y = mod.Mod(y, self.modulus)
        return (y - self.a) * self.m_1


def letter_to_number(l_):
    return alphabet.find(l_.upper())


def number_to_letter(n):
    n = int(n)
    if n == -1:
        return " "
    return alphabet[n]


if __name__ == "__main__":
    alphabet = string.ascii_uppercase
    systems = []
    for m in range(len(alphabet)):
        for a in range(len(alphabet)):
            try:
                systems.append(System(modulus=len(alphabet), m=m, a=a))
            except ValueError:
                break

    ciphertext = "GJLKT FJKXN AOTXU XAVXN KTNPJ JLKGN CYXKT WKJLP YCGJK CYJAC YHTFJ ACHAX ACNAX DANCH JA"

    table = []
    for s in systems:
        plaintext = "".join(
            number_to_letter(s.decrypt(letter_to_number(l_))) for l_ in ciphertext
        )
        if (s.m, s.a) == (9, 13):
            plaintext += " **"
        table.append([s.m, s.a, plaintext])

    print(tabulate.tabulate(table, headers=["m", "a", "plaintext"]))
