import itertools
import random


def encrypt(plaintext: bytes, key_bytes: bytes) -> bytes:
    cipher_bytes = []
    for plain, key in zip(plaintext, itertools.cycle(key_bytes)):
        cipher_bytes.append(plain ^ key)
    return bytes(cipher_bytes)


if __name__ == "__main__":
    plaintext = "Hey everyone, what's the easiest way to swindle someone?".encode(
        "iso-8859-1"
    )
    # if k is len(plaintext), then this really is a one-time pad
    random_bytes = bytes(random.choices(range(256), k=1))
    ciphertext = encrypt(plaintext, random_bytes)
    print(f"SEKRIT KEY {random_bytes.hex()}")
    print(f"CIPHERTEXT {ciphertext.hex()}")
    print(f"PLAINTEXT  {encrypt(ciphertext, random_bytes)}")
