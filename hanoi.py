import pprint


def hanoi_moves(source, dest, helper, n_disks):
    if n_disks > 0:
        yield from hanoi_moves(source, helper, dest, n_disks - 1)
        yield (source, dest)
        yield from hanoi_moves(helper, dest, source, n_disks - 1)


towers = {
    'a': list(range(10, 0, -1)),
    'b': [],
    'c': [],
}

for m in hanoi_moves(*sorted(towers.keys()), max([len(v) for v in towers.values()])):
    pprint.pprint(towers)
    source = towers[m[0]]
    dest = towers[m[1]]
    disk = source.pop()
    dest.append(disk)

pprint.pprint(towers)
