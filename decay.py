"""Demonstrate ... exponential (I guess?) decay.

The rate at which the progress bar moves (i.e., the rate at which True slots get turned into False slots) is
proportional to the number of remaining True slots, which if I recall any of my differential equations course, results
in the progress bar moving slower and slower.  Roughly, its speed is proportional to how far it is from finishing.

"""
import random

import tqdm

slots = [True] * 1_000_000

with tqdm.tqdm(total=len(slots)) as progressbar:
    while progressbar.n < progressbar.total:
        candidate = random.randint(0, len(slots) - 1)
        if slots[candidate]:
            progressbar.update()
            slots[candidate] = False
