import multiprocessing


def do_something_cpu_intensive(x):
    return x ** x


if __name__ == "__main__":
    num_processes = 8
    with multiprocessing.Pool(num_processes) as pool:
        results = [pool.apply_async(do_something_cpu_intensive, [_]) for _ in range(num_processes)]
        for r in results:
            r.wait()

        print([r.get() for r in results])
