import datetime


def is_leap_year(y):
    feb_28 = datetime.datetime(year=y, month=2, day=28)
    next_day = feb_28 + datetime.timedelta(days=1)
    return next_day.month == 2
