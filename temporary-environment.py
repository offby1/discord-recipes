import contextlib
import os


@contextlib.contextmanager
def TemporaryEnvironment(**kwargs):
    original_environment = os.environ.copy()

    os.environ.update(kwargs)

    try:
        yield
    finally:
        os.environ.clear()
        os.environ.update(original_environment)


if __name__ == "__main__":
    import subprocess

    def dump():
        child = subprocess.run(['env'], stdout=subprocess.PIPE, encoding='utf-8')
        print('-' * 80)
        print('\n'.join(sorted(child.stdout.split())))
        print()

    print("Before:")
    dump()

    with TemporaryEnvironment(foo='BAR'):
        print("During:")
        dump()

    print("After:")
    dump()
