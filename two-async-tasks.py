import asyncio


async def task_one():
    while True:
        print("Task one (the fast guy) here")
        await asyncio.sleep(0.1)


async def task_two():
    while True:
        print("Task two (slowpoke) here")
        await asyncio.sleep(1)


async def main():
    # Run for just three seconds
    await asyncio.wait([asyncio.create_task(t) for t in (task_one(), task_two())], timeout=3)


asyncio.run(main())
# python3 two-async-tasks.py
# Task one (the fast guy) here
# Task two (slowpoke) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task two (slowpoke) here
# Task one (the fast guy) here
# Task one (the fast guy) here
# Task one (the fast guy) here
