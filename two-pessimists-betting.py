"""Alice and Bob estimate the odds differently for some either-it-happens-or-it-doesnt event -- but those estimates
might both be less than 50%, or both might be more than 50%.  How can they bet on the outcome?

In prose:

* They agree on a "contract price", which is just the average of both estimated odds.

* The "optimist" -- the one with the higher estimate -- buys the contract from the pessimist, by giving them the price
  from the previous step.

* They observe the event -- either it happened, or it didn't.

* If the event occurred, then the pessimist pays the optimist 100%; otherwise they "throw out" the contract, and the
  pessimist keeps the price.

Example: the Rangers suck.  Bob says there's only a 10% chance they'll win, Alice says there's a 20% chance.
Alice gives Bob $15.
  Bob would have paid $10 for a contract that pays $100 if they win.
  Alice would have paid $20 for that same contract.
  Alice has now bought that contract, worth $20 to her, for $15.  Good deal for her.
  Bob has now sold that contract, worth $10 to him, for $15.  Good deal for him too.
The Rangers lose (of course).
No more money changes hands, so Bob is up $15, Alice is down $15. Both are happy.

If they'd won: Bob would pay Alice $100, leaving him down $85, and her up $85.
Bob is ok with this, since you can't win 'em all, and he got $15 for what he thought was worth $10.
Alice is ok, since she got $100 by spending $15, when she'd have been willing to spend $20.

"""


import dataclasses
import operator
import random

import tqdm


@dataclasses.dataclass
class Bettor:
    name: str
    estimated_odds: float
    moneez: float = 0


def _rand() -> float:
    return round(random.random(), 2)


def experiment() -> bool:
    alice = Bettor(name="Alice", estimated_odds=_rand())
    bob = Bettor(name="Bob", estimated_odds=_rand())

    pessimist, optimist = sorted(
        [alice, bob], key=operator.attrgetter("estimated_odds")
    )

    contract_price = (pessimist.estimated_odds + optimist.estimated_odds) / 2

    actual_odds = _rand()

    def closeness_to_actual_odds(b: Bettor) -> float:
        return abs(b.estimated_odds - actual_odds)

    predicted_winner = min(alice, bob, key=closeness_to_actual_odds)

    for _ in tqdm.trange(1_000_000, desc="Rolls"):
        # optimist buys contract from pessimist
        optimist.moneez -= contract_price
        pessimist.moneez += contract_price

        event_occurred = random.random() < actual_odds

        if event_occurred:
            # Optimist cashes in contract
            optimist.moneez += 1
            pessimist.moneez -= 1

    actual_winner = max(alice, bob, key=operator.attrgetter("moneez"))

    return predicted_winner == actual_winner


if __name__ == "__main__":
    experiments = [experiment() for _ in range(10)]

    as_expected = sum(experiments)
    print(f"{as_expected=} out of {len(experiments)} experiments")
