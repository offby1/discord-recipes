vowels = "aeiouy"


def starts_with_consonant(str_):
    return str_[0] not in vowels


def index_of_first_vowel(str_):
    for index, letter in enumerate(str_):
        if letter in vowels:
            return index


def pig_latin_ize(str_):
    if starts_with_consonant(str_):
        fv_index = index_of_first_vowel(str_)
        initial_consonants = str_[0:fv_index]
        return str_[fv_index:] + initial_consonants + "ay"
    else:
        return str_ + "ay"


print(pig_latin_ize("sandwich"))
print(pig_latin_ize("orange"))
