import random


def quicksort(seq):
    print(f"{seq=}")
    if len(seq) < 2:
        return seq

    pivot = random.choice(seq)
    smaller = []
    not_smaller = []
    for elt in seq:
        if elt < pivot:
            smaller.append(elt)
        else:
            not_smaller.append(elt)

    # So efficient! :-\
    if (smaller == seq) or (not_smaller == seq):
        return seq

    return quicksort(smaller) + quicksort(not_smaller)


print(
    quicksort(
        [1, 2, 3]
    )
)
