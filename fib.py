def iteratively():
    last_two = (1, 1)
    yield (last_two[0])
    while True:
        a, b = last_two
        yield b
        last_two = (b, b + a)


def fib_of_length(n, start=1):
    if n < 0:
        msg = "Sorry, can't do it!"
        raise Exception(msg)

    if n == 0:
        return []
    if n == 1:
        return [start]

    if n == 2:
        return [start, start]

    shorter = fib_of_length(n - 1, start=start)
    return [*shorter, shorter[-1] + shorter[-2]]


if __name__ == "__main__":
    print(fib_of_length(20))
    import itertools

    print(list(itertools.islice(iteratively(), 20)))
