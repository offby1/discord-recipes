def find_max_index_after(i, the_list):
    maximum = float('-inf')

    index = None
    slots_examined = i
    for number in the_list[i:]:

        if number > maximum:
            maximum = number
            index = slots_examined

        slots_examined += 1

    return (index, maximum)


def bubble_sort(the_list):
    for slots_examined in range(len(the_list)):
        i, m = find_max_index_after(slots_examined, the_list)

        the_list[slots_examined], the_list[i] = the_list[i], the_list[slots_examined]

    return None
