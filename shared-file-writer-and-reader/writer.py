import json
import os
import tempfile
import time


def atomically_write_data(data, destination_file_name):
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as outf:
        json.dump(data, outf)
    os.replace(outf.name, destination_file_name)
    print(f"Wrote {destination_file_name}")


while True:
    some_example_data = {"this": ["that", "the other"], "when": time.time()}
    atomically_write_data(some_example_data, "/tmp/shared-data-file")
    time.sleep(1)
