# cheat at https://www.nytimes.com/puzzles/spelling-bee
import collections
import pathlib

all_known_words = set()
for fn in (
    pathlib.Path(
        # https://github.com/xajkep/wordlists.git
        "~/git-repositories/3rd-party/wordlists/dictionaries/english_a-z_-_no_special_chars.txt",
    ).expanduser(),
    "/usr/share/dict/words",
):
    with open(
        fn,
    ) as inf:
        for line in inf:
            all_known_words.add(line.rstrip())

center_letter = "w"
other_six_letters = "eihdct"


for candidate in sorted(all_known_words):
    if len(candidate) < 4:
        continue
    c = collections.Counter(candidate)
    if center_letter not in c:
        continue
    haystack = set(other_six_letters).union([center_letter])
    if all(k in haystack for k in c.keys()):
        print(candidate)
