import numpy as np


class Maze:
    """A maze is a two-dimensional grid, where each cell contains either a "left" wall, a "bottom" wall, neither, or both.
    We don't bother keepting track of the right or top walls, since those belong either to the edge of the maze itself,
    or to an adjacent cell.

    """

    def __init__(self, width, height):
        self.array = np.empty(tuple((width, height)), dtype=set)

        # New mazes have all the walls.
        with np.nditer(
                self.array,
                flags=['refs_ok'],
                op_flags=['readwrite']
        ) as it:
            for x in it:
                x[...] = {"left", "bottom"}

        print(self.array)

    def knock_down_walls(self):
        starting_cell_coordinates = (0, 0)
        end_cell_coordinates = [x - 1 for x in self.array.shape]

        depth_first_search(
            starting_cell_coordinates,
            shuffled_unvisited_neighbors,
            end_cell_coordinates
        )


if __name__ == "__main__":
    m = Maze(3, 2)
