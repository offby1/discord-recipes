# make_bricks(3, 1, 8) → True
# make_bricks(3, 1, 9) → False
# make_bricks(3, 2, 10) → True

def make_bricks(one_inch, five_inch, goal_length, depth=0):
    def dprint(str_):
        print(' ' * depth + str_)

    dprint(f"{one_inch=} {five_inch=} {goal_length=}")

    if one_inch < 0 or five_inch < 0:
        dprint("bzzt, out of bricks")
        return False

    if goal_length == 0:
        dprint("woohoo")
        return True

    if goal_length < 0:
        dprint("bzzt, negative goal")
        return False

    from_trying_five = make_bricks(one_inch, five_inch - 1, goal_length - 5, depth + 1)
    if from_trying_five:
        return from_trying_five

    return make_bricks(one_inch - 1, five_inch, goal_length - 1, depth + 1)


for trial, expected in (
        [(3, 1, 8), True],
        [(3, 1, 9), False],
        [(3, 2, 10), True],
        [(3, 7, 10), True],
        [(4, 1, 10), False],
        [(5, 1, 10), True],
):
    actual = make_bricks(*trial)
    assert actual == expected, f"make_bricks_slowly{trial}"

print("All tests passed, Boss")
