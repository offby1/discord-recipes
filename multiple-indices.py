import collections
import csv
import dataclasses
import pprint


geo_data_csv = """
Country, City, Latitude, Longitude, FlagShape
Arabia, Mecca, 0, 0, rectangular
Arabia, Medina, 32, 32, rectangular
America, Washington, 64, 64, round
"""

GeoData = None

list_of_geo_thingies = []
csv_reader = csv.reader(geo_data_csv.strip().split('\n'))

for row in csv_reader:
    if len(row):
        row = [thing.strip() for thing in row]

        # first non-empty row holds the field names ...
        if GeoData is None:
            GeoData = dataclasses.make_dataclass("GeoData", row, frozen=True)
        # subsequent rows hold actual data.
        else:
            list_of_geo_thingies.append(GeoData(*row))
    else:
        if GeoData is not None:
            print(f"Warning -- cannot convert CSV {row=} because it doesn't have exactly {len(dataclasses.fields(GeoData))} fields")
        else:
            print(f"Warning -- cannot convert CSV {row=} because ... I haven't yet figured out how the class should look")


dicts_by_index_name = collections.defaultdict(lambda: collections.defaultdict(set))

for g in list_of_geo_thingies:
    for field in dataclasses.fields(GeoData):
        dicts_by_index_name[field.name][getattr(g, field.name)].add(g)

pprint.pprint(dicts_by_index_name)
