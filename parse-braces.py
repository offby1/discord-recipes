def parse_braces(str_):
    brace_pairs = (
        "()",
        "{}",
        "[]",
        "<>",
    )
    closing_brace_by_opening_brace = {p[0]: p[1] for p in brace_pairs}
    opening_brace_by_closing_brace = {p[1]: p[0] for p in brace_pairs}

    stack = []

    for c in str_:
        if c in closing_brace_by_opening_brace:
            stack.append(c)
        else:
            if o := opening_brace_by_closing_brace.get(c):
                if stack and (stack[-1] == o):
                    stack.pop()
                else:
                    print(
                        f"Mismatched {c!r} ({repr(stack[-1]) if stack else 'no brace'} is currently open)",
                    )
                    return False

    if stack:
        print(f"Unmatched opening braces at end of string: {stack!r}")
        return False

    return True


assert parse_braces("")
assert not (parse_braces("({[)"))
assert not (parse_braces(")"))
assert parse_braces("()")
assert not (parse_braces("(){"))
assert parse_braces("(){}")
assert not (parse_braces("{(){}"))
assert not (parse_braces("{(){}x(})"))
assert parse_braces("{(){}x}")
assert parse_braces("{(){}}")

print("all tests passed!")
