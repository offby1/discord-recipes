import time

# https://apscheduler.readthedocs.io/en/stable/
from apscheduler.schedulers.background import \
    BackgroundScheduler  # python3 -m pip install apscheduler


def background_thing():
    print("Look, ma, I'm running in Da Background")


sched = BackgroundScheduler()
sched.add_job(background_thing, 'interval', seconds=0.5)
sched.start()

while True:
    print("Burp")
    time.sleep(0.1)
