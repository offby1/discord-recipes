import datetime
import http
import os
import time

import flask

# Run me like this
"""
$ cd this-directory
$ python3 -m flask run --reload
"""

os.environ["TZ"] = "utc"
time.tzset()


def my_log_date_time_string(self):
    """Return the current time formatted for logging.

    Intended to override the log_date_time_string method of BaseHTTPRequestHandler, which might be standard, but is
    horrible.

    """
    return datetime.datetime.now(tz=datetime.timezone.utc).strftime("%FT%T%z")


http.server.BaseHTTPRequestHandler.log_date_time_string = my_log_date_time_string

app = flask.Flask(__name__)


@app.route("/")
def index():
    now = datetime.datetime.now(tz=datetime.timezone.utc)

    return flask.render_template_string(
        """
<!doctype html>
<script type="text/javascript">
function localize(t)
{
  var d=new Date(t);
  document.write(d.toString());
}
</script>
<title>Hello from Flask</title>
<h1>Hey thar {{ name }} from {{ remote_ip }}!</h1>
<h2>A few words about time</h2>
        <p>
        The time here in flask land is {{ now.isoformat() }}</p>
        <p>That's
        <script type="text/javascript">localize("{{now.isoformat()}}");</script> in your local time zone
        </p>
{% for user in users %}
  <li><a href="{{ user }}">{{ loop.index }}: {{ user }}</a></li>
{% endfor %}
""",
        name="Buddy",
        now=now,
        remote_ip=flask.request.remote_addr,
        users=["mick", "keith", "charlie"],
    )


@app.route("/<user>")
def user(user):
    return f"User is {user}"
