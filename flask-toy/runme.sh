#!/bin/sh

here="$(cd "$(dirname "$0")" && pwd)"
set -x

cd "${here}"

set -e

${here}/.venv/bin/python3 -m flask run --debug
