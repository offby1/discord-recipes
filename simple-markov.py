import collections
import random

import requests

text = (
    requests.get(
        "https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt"
    )
    .text.replace("\n", " ")
    .replace("\t", " ")
    .replace("/", " ")
    .replace("\\", " ")
    .replace("*", " ")
    .replace("#", " ")
).split()


successor_words = collections.defaultdict(collections.Counter)
for initial, successor in zip(text, text[1:]):
    successor_words[initial][successor] += 1

for word, counter in successor_words.items():
    successor_words[word] = dict(counter.most_common(n=10))


def random_successor(word):
    all_successors = successor_words.get(word)
    if not all_successors:
        return None

    return random.choices(
        list(all_successors.keys()), weights=all_successors.values(), k=1
    )[0]


initial_word = "king"
print(initial_word, end=" ")
for _ in range(20):
    successor = random_successor(initial_word)
    if not successor:
        break
    print(successor, end=" ")
    initial_word = successor

print()
