import collections
import datetime
import random


def experiment(group_size) -> bool:
    bday_counts = collections.Counter()

    for _i in range(group_size):
        random_date = datetime.datetime(2000, 1, 1) + datetime.timedelta(days=random.randrange(0, 366))

        bday = (random_date.month, random_date.day)
        bday_counts[bday] += 1

    [(_elt, n_occurences)] = bday_counts.most_common(1)
    return n_occurences > 1


for group_size in range(1, 100):
    successful_experiments = 0
    for _round in range(1000):
        if experiment(group_size):
            successful_experiments += 1
    print(f"{group_size=} {successful_experiments / 1000=}")
