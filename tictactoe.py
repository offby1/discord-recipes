import collections
import dataclasses
import random
from typing import Dict, Iterator, Optional

import tabulate
from termcolor import colored  # pip install termcolor


def same_row_coordinates(loc: "Location") -> Iterator["Location"]:
    for col in range(loc.board.num_cols):
        yield Location(col, loc.row, loc.board)


def same_col_coordinates(loc: "Location") -> Iterator["Location"]:
    for row in range(loc.board.num_rows):
        yield Location(loc.column, row, loc.board)


# North and South are the opposite of what you think, since 0, 0 is in the NorthWest corner
def nw_se_diagonal_coordinates(loc: "Location") -> Iterator["Location"]:
    if loc.row - loc.column == 0:
        for i in range(loc.board.num_rows):
            yield Location(i, i, loc.board)


def ne_sw_diagonal_coordinates(loc: "Location") -> Iterator["Location"]:
    if loc.row + loc.column == loc.board.num_rows - 1:
        for i in range(loc.board.num_rows):
            yield Location(i, loc.board.num_rows - 1 - i, loc.board)


class BoardFull(Exception):
    def __init__(self, board: "Board") -> None:
        self.board = board
        super()

    def __str__(self) -> str:
        return "board is full:\n" + str(self.board)


@dataclasses.dataclass
class Location:
    column: int
    row: int
    board: "Board"

    def __hash__(self) -> int:
        return hash((self.column, self.row))

    @classmethod
    def get_valid_choice(kls, board: "Board") -> "Location":
        if board.is_full():
            raise BoardFull(board)

        while True:
            column = random.randint(0, board.num_cols - 1)
            row = random.randint(0, board.num_rows - 1)

            candidate = kls(column, row, board)
            whats_there = board.get_at(candidate)
            if whats_there == " ":
                return candidate


@dataclasses.dataclass
class Player:
    name: str
    token: str

    def __str__(self) -> str:
        return f"{self.name} playing {self.token}"


class Board:
    def __init__(self) -> None:
        self.num_rows = 3       # values other than 3 work fine
        self.tokens_by_location: Dict[Location, str] = collections.defaultdict(
            lambda: " "
        )

    @property
    def num_cols(self) -> int:
        return self.num_rows

    def is_full(self) -> bool:
        for t in self.tokens_by_location.values():
            if t == " ":
                return False

        return True

    def mark_winning_plays(self, player_token, location_generator):
        for loc in location_generator:
            self.tokens_by_location[loc] = colored(player_token, "green")

    def mark_for_player(self, p: Player, location: Location) -> Optional[Player]:
        self.tokens_by_location[location] = p.token
        for check in (
            same_row_coordinates,
            same_col_coordinates,
            ne_sw_diagonal_coordinates,
            nw_se_diagonal_coordinates,
        ):
            other_cells_from_this_player = [
                self.get_at(cell) == p.token for cell in check(location)
            ]

            if len(other_cells_from_this_player) == self.num_rows and all(
                other_cells_from_this_player
            ):
                self.mark_winning_plays(p.token, check(location))
                print(f"Ooh ooh {p} just won via {check.__name__}")
                return p

        return None

    def get_at(self, location: Location) -> str:
        return self.tokens_by_location[location]

    def row_tokens(self, row: int) -> list[str]:
        return [
            self.tokens_by_location[Location(column, row, self)]
            for column in range(self.num_cols)
        ]

    def __str__(self) -> str:
        return tabulate.tabulate(
            [self.row_tokens(row) for row in range(self.num_rows)],
            tablefmt="fancy_grid",
        )


def main_loop() -> None:
    b = Board()

    players = [Player("bob", "X"), Player("alice", "O")]
    random.shuffle(players)     # I suspect the player who goes first has a big advantage

    while True:
        for p in players:
            print()
            print(b)
            print(f"{p}'s turn")
            try:
                location = Location.get_valid_choice(b)
            except BoardFull:
                print("So it's a tie")
                return
            winner = b.mark_for_player(p, location)
            if winner:
                print(b)
                return


if __name__ == "__main__":
    main_loop()
