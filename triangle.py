def one_row(num_spaces, num_stars):
    print(" " * num_spaces, end="")
    print("*" * num_stars)


def triangle(height):

    for row in range(1, height + 1):
        num_spaces = height - row
        num_stars = 2 * row - 1

        one_row(num_spaces, num_stars)


def numeric_triangle(half_height):
    def one_row(middle_number):
        increasing_columns = list([str(i) for i in range(1, middle_number + 1)])
        decreasing_columns = list(reversed(increasing_columns))[1:]
        return " ".join(increasing_columns + decreasing_columns)

    max_width = half_height * 4 - 3

    def left_pad(s):
        wat = (max_width - len(s)) // 2
        return (" " * wat) + s

    increasing_rows = [left_pad(one_row(r)) for r in range(1, half_height + 1)]
    decreasing_rows = list(reversed(increasing_rows))[1:]

    for r in increasing_rows + decreasing_rows:
        print(r)


if __name__ == "__main__":
    triangle(6)
    numeric_triangle(6)
