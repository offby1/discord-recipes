import collections
import csv


class Deduper:
    """
    Yields the latest entry for each date.
    """
    def __init__(self):
        self.data_by_date = collections.defaultdict(list)

    def ingest_data(self, data):
        self.data_by_date[data["date"]].append(data)

    def deduped(self):
        for date in sorted(self.data_by_date.keys()):
            yield self.data_by_date[date][-1]


def deduped_lines(input_rows):
    deduper = Deduper()
    for row in input_rows:
        deduper.ingest_data(row)

    yield from deduper.deduped()


columns = "date this that the-other".split()
with open("input.txt") as inf:
    reader = csv.DictReader(inf, columns)
    with open("output.txt", "w") as outf:
        writer = csv.DictWriter(outf, columns)
        next(reader)            # skip the header
        for line in deduped_lines(reader):
            writer.writerow(line)
