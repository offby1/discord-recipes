import functools
import time

def decorator(some_integer):
    def middle_thingy(original_function):
        @functools.wraps(original_function)
        def inner_thingy(*args, **kwargs):
            print(f"about to call {original_function=} with {args=} {kwargs=}")
            try:
                rv = original_function(*args, **kwargs)
            except Exception as e:
                print(f"{original_function=} raised {e=}")
                raise
            else:
                print(f"{original_function=} returned {rv=}")

            return rv

        return inner_thingy

    return middle_thingy


@decorator(6)
def some_function(*args, **kwargs):
    print(f"Hello, I got {args=} and {kwargs=}")
    if int(time.time()) % 2:
        raise Exception("It's beer o'clock!")
    return sum(args)


if __name__ == "__main__":
    some_function(1, 2, 3, hello="kitty")
