import datetime

import tabulate


def humanize_interval_seconds(num_seconds: float) -> str:
    num_seconds = int(round(num_seconds))
    minutes, seconds = divmod(num_seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    weeks, days = divmod(days, 7)

    return f"{weeks}w {days:2}d {hours:2}h {minutes:2}m {seconds:2}s"


def humanize_interval_timedelta(td: datetime.timedelta) -> str:
    as_string = str(td)

    hours, minutes, seconds = as_string.split(":")

    return f"{hours}h {minutes}m {seconds}s"


seconds = 1
table = []
while seconds < 10_000_000:
    td = datetime.timedelta(seconds=seconds)
    table.append(
        [seconds, humanize_interval_seconds(seconds), humanize_interval_timedelta(td)]
    )
    seconds *= 11

print(
    tabulate.tabulate(
        table, headers=["seconds", "one way", "another way"], stralign="right"
    )
)
