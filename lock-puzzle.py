import sys

# See lock-puzzle.png in this directory for the problem statement, and lock-puzzle-test-cases for, you know, test cases


def categorize_common_digits(n1, n2):
    digits_1 = list("{:03}".format(n1))
    digits_2 = list("{:03}".format(n2))

    common_and_same_location = set()
    common_but_differing_location = set(digits_1).intersection(set(digits_2))

    for d1, d2, in zip(digits_1, digits_2):
        if d1 == d2:
            common_and_same_location.add(d1)
            common_but_differing_location.remove(d1)

    return common_and_same_location, common_but_differing_location


def checks_out(number, clue_strings):
    for clue_string in clue_strings:
        digits, expected_right, expected_wrong = (int(s) for s in clue_string.split('-'))

        right, wrong = categorize_common_digits(number, digits)

        if len(right) != expected_right:
            return False
        if len(wrong) != expected_wrong:
            return False

    return True


if __name__ == "__main__":
    clues = sys.argv[1:]

    for n in range(0, 1000):

        if not checks_out(n, clues):
            continue

        print(f"Maybe {n=} is it")

# python3 lock-puzzle.py 682-1-0 614-0-1 206-0-2 738-0-0 380-0-1 314-0-1
# Maybe n=42 is it
