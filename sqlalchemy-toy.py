# following along https://docs.sqlalchemy.org/en/14/tutorial/engine.html
import datetime
import operator
import uuid

from sqlalchemy import Column, ForeignKey, String, create_engine, select
from sqlalchemy.orm import Session, registry, relationship
from sqlalchemy_utc import UtcDateTime  # pip install SQLAlchemy-Utc

# PATH="/usr/local/opt/postgresql@12/bin:$PATH" python3 -m pip install psycopg2
# docker run  -p 5432:5432 -e POSTGRES_HOST_AUTH_METHOD=trust postgres:12

# On a normal host, had to edit a postgres server file to get this to work (https://stackoverflow.com/a/26898164);
# otherwise it complains about missing password
# engine = create_engine("postgresql+psycopg2://ubuntu@localhost:5432/sqlalchemy", echo=True, future=True)
engine = create_engine("sqlite+pysqlite:////tmp/db.db", echo=True, future=True)

mapper_registry = registry()
Base = mapper_registry.generate_base()


class User(Base):
    __tablename__ = "users"

    name = Column(String(30), primary_key=True)
    signup = Column(UtcDateTime, default=lambda: datetime.datetime.now(tz=datetime.timezone.utc))

    posts = relationship("Post", back_populates="user")

    def __repr__(self):
        if self.signup is None:
            signup = '?'
        else:
            signup = self.signup.isoformat()
        return f"User(name={self.name}, signup={signup})"


class Post(Base):
    __tablename__ = "posts"

    id = Column(String(36), primary_key=True, default=lambda: str(uuid.uuid4()))
    title = Column(String(30))
    text = Column(String)
    created = Column(UtcDateTime, default=lambda: datetime.datetime.now(tz=datetime.timezone.utc))
    user_id = Column(ForeignKey('users.name'), nullable=False)

    user = relationship("User", back_populates="posts")

    def __repr__(self):
        if self.created is None:
            created = '?'
        else:
            created = self.created.isoformat()
        return f"{self.title}: by {self.user.name} on {created}"


mapper_registry.metadata.create_all(engine)

with Session(engine) as session:
    desired_users = set(("sam", "carol", "david"))
    actual_users = session.execute(select(User)).all()

    for username in desired_users - set([u[0].name for u in actual_users]):
        u = User(name=username)
        session.add(u)
        print(f"Creating user {vars(u)}")

    session.commit()

    for u in session.execute(select(User)).all():
        u = u[0]
        for i in (1, 2):
            session.add(
                Post(
                    title=f"{u.name} is so great, part {i}",
                    text=(f"All work and no play makes {u.name} a dull boy. " * 5)
                    if i == 1
                    else (f"{u.name} is so great!! " * 5),
                    user=u,
                )
            )

    session.commit()
    for u in session.execute(select(User)):
        u = u[0]
        print(f"{u} has {len(u.posts)} posts; oldest is ", end='')
        print(min(u.posts, key=operator.attrgetter('created')))
