import itertools
import pprint
import random

# This is a Python version of something I wrote in elisp(!) back in the late '80s -- possibly the first actually-useful
# code I ever wrote.  The idea was: we were selling software on floppy disks; the software was a bunch of files; we
# wanted to not only use the fewest disks, but also to "pack" the disks evenly (so that if, at the last minute, we
# realized we need to add another file to Disk Seven, there'd likely be a bit of leftover space on it).
#
# I think it took me hours or days to do it in elisp; today, in python, this took ... I dunno ... five minutes?
#
# Compare to
# https://gitlab.com/offby1/doodles/-/blob/58a5c11117dd5d6befcb801642accb7af33a2890/random-scheme-stuff/triumph.scm from
# 2006, which I bet took me a couple hours.


class Container(list):
    def __repr__(self):
        return f"{sum(self)}: {list.__repr__(self)}"


def empty_containers(n):
    return [Container() for _ in range(n)]


def randomly_sized_items(num_items=20):
    return [int(round(random.uniform(0, 100))) for _ in range(num_items)]


def emptiest(containers):
    return min(containers, key=sum)


evenly_packed_containers = empty_containers(6)
randomly_packed_containers = itertools.cycle(empty_containers(6))

items = randomly_sized_items()

for i in sorted(items, reverse=True):
    c = emptiest(evenly_packed_containers)
    c.append(i)
    rpc = next(randomly_packed_containers)
    rpc.append(i)

print(f"Even:::\n{pprint.pformat(sorted(evenly_packed_containers, key=sum))}")
print(
    f"""Random:::\n{pprint.pformat(
    sorted(
        itertools.islice(randomly_packed_containers, len(evenly_packed_containers)),
        key=sum,
    )
)}"""
)
