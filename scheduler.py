import datetime
import logging
import os
import pathlib
import random
import sched
import sys
import threading
import time

os.environ['TZ'] = 'utc'
time.tzset()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(datefmt='%FT%T%z', format='%(asctime)s:%(levelname)s:%(name)s: %(message)s')


def hello_world(x, y):
    logger.info(f"{x} Hello, world {y=}")


def do_stuff_on_a_schedule(status_file):
    s = sched.scheduler(time.time, time.sleep)

    now = round(time.time())

    while len(s.queue) < 20:
        when = now + random.randint(0, 20)
        priority = random.randint(1, 3)
        when_str = datetime.datetime.fromtimestamp(when, tz=datetime.timezone.utc)
        s.enterabs(when, priority, lambda when_str=when_str, priority=priority: hello_world(when_str, priority))

    t = threading.Thread(target=s.run)
    t.start()
    while True:
        status_file.seek(0)
        print(f"{len(s.queue)} items in scheduler queue", file=status_file)
        status_file.flush()
        status_file.truncate()

        if not t.is_alive():
            break

        time.sleep(0.5)
    t.join()


if __name__ == "__main__":
    status_file_name = pathlib.Path('/tmp/status')

    if len(sys.argv) > 1:
        if sys.argv[1] == "-status":
            with open(status_file_name) as inf:
                print(inf.read())
    else:
        with open(status_file_name, 'w') as outf:
            print(f"Will write status to {outf=}")
            do_stuff_on_a_schedule(outf)
