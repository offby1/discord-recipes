# https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher

ALPHABET = ''.join([chr(c) for c in range(256) if chr(c).isalnum()])

# ensure it's an even length
if len(ALPHABET) % 2:
    ALPHABET = ALPHABET[:-1]

# Yeah yeah it's not "13" but whatever.
ROT_13_PIVOT = ALPHABET[len(ALPHABET) // 2]


def repeat_keyword(keyword, desired_length):
    result = ''
    while len(result) < desired_length:
        result += keyword
    return result


def add_letters(l1, l2):
    if l1 not in ALPHABET:
        return l1
    if l2 not in ALPHABET:
        return l1

    index1 = ALPHABET.index(l1)
    index2 = ALPHABET.index(l2)

    sum = index1 + index2
    remainder = sum % len(ALPHABET)
    return ALPHABET[remainder]


def encrypt(plaintext, keyword):
    ciphertext = ''
    mask = repeat_keyword(keyword, len(plaintext))

    for plain, key in zip(plaintext, mask):
        ciphertext += add_letters(plain, key)

    return ciphertext


if __name__ == "__main__":
    plaintext = "wassup homies"
    ciphertext = encrypt(plaintext, ROT_13_PIVOT)
    print(f"{plaintext=} {ciphertext=}")
    print(f"Recovered plaintext: {encrypt(ciphertext, ROT_13_PIVOT)}")
