# From ammar#8281 on discord

from tkinter import Tk, Entry, END, Button

root = Tk()
root.title("Calculator")

e = Entry(root, width=35, borderwidth=5)
e.grid(row=0, column=0, columnspan=3, padx=10, pady=10)


def enter_digit_button(number):
    adx = e.get()
    e.delete(0, END)
    e.insert(0, str(adx) + str(number))


def clear_command():
    e.delete(0, END)


def plus_command():
    m1 = e.get()
    global lol
    global math
    math = "sabiranje"
    lol = float(m1)
    e.delete(0, END)


def equals_command():
    global mrav
    mrav = e.get()
    e.delete(0, END)
    if math == "sabiranje":
        e.insert(0, lol + float(mrav))

    if math == "oduzimanje":
        e.insert(0, lol - float(mrav))

    if math == "puta":
        e.insert(0, lol * float(mrav))

    if math == "dijeljenje":
        e.insert(0, lol / float(mrav))


def minus_command():
    m1 = e.get()
    global lol
    global math
    math = "oduzimanje"
    lol = float(m1)
    e.delete(0, END)


def times_command():
    m1 = e.get()
    global lol
    global math
    math = "puta"
    lol = float(m1)
    e.delete(0, END)


def divide_command():
    m1 = e.get()
    global lol
    global math
    math = "dijeljenje"
    lol = float(m1)
    e.delete(0, END)


digit_buttons = [
    Button(root, text=str(i), padx=30, pady=8, command=lambda i=i: enter_digit_button(i))
    for i in range(10)
]

for db in digit_buttons:
    t = db.config()["text"]

    number = int(t[-1])
    if number == 0:
        row = 4
        column = 0
    else:
        row = (12 - number) // 3
        column = number % 3

    db.grid(row=row, column=column)


plus_button = Button(root, text='+', padx=29, pady=8, command=plus_command)
equals_button = Button(root, text="=", padx=29, pady=8, command=equals_command)
clear_button = Button(root, text="Clear", padx=100, pady=8, command=clear_command)

minus_button = Button(root, text="-", padx=30, pady=8, command=minus_command)
times_button = Button(root, text="*", padx=31, pady=8, command=times_command)
colon_button = Button(root, text=":", padx=32, pady=8, command=divide_command)

minus_button.grid(row=5, column=0, columnspan=1)
times_button.grid(row=5, column=1, columnspan=1)
colon_button.grid(row=5, column=2, columnspan=1)

plus_button.grid(row=4, column=1)
equals_button.grid(row=4, column=2)
clear_button.grid(row=6, column=0, columnspan=10)

root.mainloop()
