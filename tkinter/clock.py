import datetime
from tkinter import BooleanVar, Checkbutton, Label, Tk, font

root = Tk()
root.title("Tick Tock")

the_font = font.nametofont("TkFixedFont")
the_font.configure(size=75)

label = Label(root, font=the_font)
label.grid(row=0, column=0)


Running = BooleanVar()
Running.set(True)


def resume_or_pause() -> None:
    if Running.get():
        root.after(0, update_time)


resume_checkbox = Checkbutton(
    text="Running",
    state="active",
    variable=Running,
    command=resume_or_pause,
)
resume_checkbox.grid(row=1, column=0)


def update_time() -> None:
    if not Running.get():
        return

    now = datetime.datetime.now(tz=datetime.timezone.utc)
    label["text"] = now.replace(microsecond=0).isoformat()

    ms_until_next_second = (1_000_000 - now.microsecond) // 1000
    root.after(ms_until_next_second, update_time)


root.after(0, update_time)
root.mainloop()
