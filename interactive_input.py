# See also https://github.com/sfischer13/python-prompt
# and https://pyinputplus.readthedocs.io/en/latest/
import datetime
import decimal

import dateparser  # pip install dateparser


def get_int(prompt: str) -> int:
    while True:
        try:
            return int(input(prompt))
        except ValueError as e:
            print(f"{e}; please try again")


def get_bounded_int(prompt: str, smallest: int, largest: int) -> int:
    while True:
        datum = get_int(prompt)

        if smallest <= datum <= largest:
            return datum

        print(f"{datum} is not between {smallest} and {largest}")


def get_space_separated_sequence_of_ints(prompt: str) -> list[int]:
    while True:
        items_they_typed = input(prompt).split()
        try:
            return [int(i) for i in items_they_typed]
        except ValueError as e:
            print(f"{e}; please try again")


def get_float(prompt: str) -> float:
    while True:
        try:
            return float(input(prompt))
        except ValueError as e:
            print(f"{e}; please try again")


def get_decimal(prompt: str) -> decimal.Decimal:
    while True:
        try:
            return decimal.Decimal(input(prompt))
        except Exception as e:
            print(f"{e}; please try again")


def get_fraction(prompt: str) -> float:
    while True:
        while True:
            try:
                f = float(input(prompt))
            except ValueError as e:
                print(f"{e}; please try again")
            else:
                break

        if 0 <= f < 1:
            return f

        print(f"{f} is not between 0 and 1")


# menu
def get_key_from_menu_dict(prompt: str, choices: dict[str, str]) -> str:
    print(prompt)
    keys_as_string = ", ".join(repr(k) for k in choices)
    while True:
        for key, value in choices.items():
            print(f"{key}: {value}")
        datum = input(f"Enter one of {keys_as_string}: ").strip()
        if datum in choices:
            return choices[datum]
        print(f"{datum} isn't in {keys_as_string}; please try again")


def get_string_from_choices(prompt: str, choices: list[str]) -> str:
    while True:
        datum = input(prompt).strip()
        if datum in choices:
            return datum
        print(f"{datum} isn't in {choices}; please try again")


def get_yes_no(prompt: str) -> bool:
    while True:
        datum = input(prompt).strip()
        if datum and datum[0].lower() in "yn":
            return datum[0].lower() == "y"

        print(f"{datum} doesn't begin with 'y' or 'n'; please try again")


def get_date(prompt: str) -> datetime.datetime:
    today = datetime.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

    while True:
        date_string = input(prompt + " (YYYY-mm-dd)? ").strip()

        parsed = dateparser.parse(date_string, settings={"RELATIVE_BASE": today})
        if parsed is not None:
            return parsed

        print(f"Couldn't parse {date_string} as a date; sorry")
