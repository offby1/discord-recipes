import collections
import datetime
import pprint

some_day = datetime.datetime.now().date().replace(day=1)
wednsdays_by_year_and_month = collections.defaultdict(int)

while True:
    y, m = some_day.year, some_day.month

    if some_day.weekday() == 2:  # sure wish they had enums for these values
        wednsdays_by_year_and_month[some_day.strftime("%Y-%m (%B)")] += 1

    some_day += datetime.timedelta(days=1)

    if some_day.day == 1 and len(wednsdays_by_year_and_month) == 20:
        break

pprint.pprint(dict(wednsdays_by_year_and_month))
