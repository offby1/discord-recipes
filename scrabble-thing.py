import operator

scores = {
    "a": 1,
    "c": 3,
    "b": 3,
    "e": 1,
    "d": 2,
    "g": 2,
    "f": 4,
    "i": 1,
    "h": 4,
    "k": 5,
    "j": 8,
    "m": 3,
    "l": 1,
    "o": 1,
    "n": 1,
    "q": 10,
    "p": 3,
    "s": 1,
    "r": 1,
    "u": 1,
    "t": 1,
    "w": 4,
    "v": 4,
    "y": 4,
    "x": 8,
    "z": 10,
}


def word_score(word):
    """Calculate the score of a given word."""
    return sum(scores.get(x, 0) for x in word)


def load_dictionary():
    """Create the variable containing the wordlist list of words."""
    with open("/usr/share/dict/words", "r") as inf:
        return [line.rstrip().lower() for line in inf.readlines()]


dictionary = load_dictionary()
scores_by_word = [(w, word_score(w)) for w in dictionary]
best_word = max(scores_by_word, key=operator.itemgetter(1))

print(best_word)
