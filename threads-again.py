import queue
import random
import threading
import time

the_input_queue = queue.Queue()


completed_work_items = []


def checker(checkstand_number):
    my_work_done = []
    my_start_time = time.time()
    while not the_input_queue.empty():
        customer, wait_time = the_input_queue.get()
        print(f"{customer=} {wait_time=} joins {checkstand_number=}")
        time.sleep(wait_time)
        my_work_done.append(wait_time)
        print(f"{customer=} is done at {checkstand_number}")
        completed_work_items.append(wait_time)
    my_stop_time = time.time()
    print(
        f"{threading.current_thread().name} completed {len(my_work_done)} wozzits in {my_stop_time - my_start_time} seconds",
    )
    speed = len(my_work_done) / (my_stop_time - my_start_time)
    other_speed = sum(my_work_done) / (my_stop_time - my_start_time)
    print(
        f"{threading.current_thread().name}: {speed} items per second; {other_speed} work units per second",
    )


for customer_number in range(100):
    this_customers_wait_time = random.choice(range(3, 11)) / 100
    the_input_queue.put((customer_number, this_customers_wait_time))


threads = []
start_time = time.time()
for _ in range(5):
    checker_thread = threading.Thread(
        target=checker,
        args=(_,),
        name=f"checkstand #{_}",
    )
    checker_thread.start()

    threads.append(checker_thread)

for t in threads:
    t.join()
stop_time = time.time()

print(
    f"It took {stop_time - start_time} seconds to do {sum(completed_work_items)} work units: {(sum(completed_work_items) / (stop_time - start_time)):2g} per second",
)
