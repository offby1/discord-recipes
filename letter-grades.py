import bisect
import operator
import sys
from typing import Any, Callable, Tuple

import tabulate

if sys.version_info < (3, 10):
    raise Exception(
        f"Sorry, I need at least python 3.10 but this is {sys.version.split(maxsplit=1)[0]}"
    )

grades_table = [
    (0, "FLUNNNNKKKKK"),
    (64, "D"),
    (67, "D+"),
    (70, "C-"),
    (74, "C"),
    (77, "C+"),
    (80, "B-"),
    (84, "B"),
    (87, "B+"),
    (90, "A-"),
    (94, "A"),
    (99, "A+"),
]


# https://docs.python.org/3/library/bisect.html#searching-sorted-lists
def find_le(
    a: list[Tuple[int, str]], x: int, key: Callable[[Any], Any]
) -> Tuple[int, str]:
    "Find rightmost value less than or equal to x"
    i = bisect.bisect_right(a, x, key=key)  # the "key" kwarg is new in 3.10
    if i:
        return a[i - 1]

    raise ValueError(f"Can't find {x=} in {a=} using {key=}")


def score_to_letter_grade(score: int) -> Tuple[int, str]:
    return find_le(grades_table, score, key=operator.itemgetter(0))


table = []
for score in range(0, 101, 5):
    table.append((score, score_to_letter_grade(score)[1]))

print(tabulate.tabulate(table, headers=["score", "grade"]))
