import logging
import os
import threading
import time

os.environ["TZ"] = "utc"
time.tzset()

the_event = threading.Event()

_log = logging.getLogger(__name__)


def worker():
    while True:
        the_event.wait()
        _log.info("Worker thread doing some work!")
        time.sleep(0.1)


def main():
    t = threading.Thread(target=worker, daemon=True)
    t.start()

    the_event.set()
    _log.info("Let's let the worker run a while")
    time.sleep(0.6)

    _log.info("Now we pause it")
    the_event.clear()
    time.sleep(3)

    _log.info("Now we let it resume")
    the_event.set()
    time.sleep(0.6)

    _log.info("Outta here!  C Ya")


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)5s [%(thread)d] %(message)s",
        level=logging.DEBUG,
        datefmt="%Y-%m-%dT%H:%M:%S%z",
    )
    main()
