from typing import Iterator, List


def intersperse(item: str, seq: List[str]) -> Iterator[List[str]]:
    """
    >>> list(intersperse("x", ["1", "2", "3"]))
    [['x', '1', '2', '3'], ['1', 'x', '2', '3'], ['1', '2', 'x', '3'], ['1', '2', '3', 'x']]
    """
    for index in range(len(seq)):
        yield seq[0:index] + [item] + seq[index:]
    yield seq + [item]


def permutations(seq: List[str]) -> List[List[str]]:
    if not seq:
        return []
    if len(seq) == 1:
        return [seq]

    smaller = seq[1:]
    from_smaller = permutations(smaller)
    result: List[List[str]] = []
    for perm in from_smaller:
        result.extend(intersperse(seq[0], perm))
    return result
