from collections.abc import Sequence
import dataclasses
import operator
import pprint
import itertools
from typing import Generator


@dataclasses.dataclass
class Recipe:
    name: str
    cooking_time: int


class Order:
    def __init__(self, recipes: Sequence[Recipe]):
        self.recipes = recipes
        self.cooking_time : int = sum(r.cooking_time for r in self.recipes)

    def __iter__(self):
        return iter(self.recipes)

    def __repr__(self):
        recipes = "; ".join([repr(recipe) for recipe in self.recipes])
        return f"{self.cooking_time=}: {recipes}"


def knapsack_solutions(knapsack_size: int, items: Sequence[Recipe]) -> Generator[Order, None, None]:
    for n in range(1, len(items) + 1):
        for wat in itertools.combinations(items, n):
            candidate = Order(wat)
            if candidate.cooking_time <= knapsack_size:
                yield candidate


cooking_times = [
    Recipe(name=name, cooking_time=cooking_time) for name, cooking_time in {
        'Bulgogi': 75,
        'Crème Brûlée': 50,
        'Gambas al Ajillo': 20,
        'Gourmet Mushroom Risotto': 50,
        'Grilled Cheese Sandwich': 20,
        'Guacamole': 10,
        'Khao Neeo Mamuang': 90,
        'Miso Soup': 20,
        'Paratha': 150,
        'Shrimp Fried Rice': 30
    }.items()
]


pprint.pprint(sorted(knapsack_solutions(50, cooking_times), key=operator.attrgetter('cooking_time')))
