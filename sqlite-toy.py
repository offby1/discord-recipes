import datetime
import sqlite3

import tabulate  # python3 -m pip install tabulate

conn = sqlite3.connect(":memory:")
conn.row_factory = sqlite3.Row

column_names = ["reason", "value", "timestamp"]

c = conn.cursor()
c.execute(f"CREATE TABLE  IF NOT EXISTS topics ({', '.join(column_names)})")
c.execute("CREATE INDEX IF NOT EXISTS time_index ON topics(timestamp)")

now = datetime.datetime.now(tz=datetime.timezone.utc)
for word in ("hello", "there", "cousin"):
    # "qmark" placeholder -- https://docs.python.org/3/library/sqlite3.html#how-to-use-placeholders-to-bind-values-in-sql-queries
    c.execute(
        f"INSERT INTO topics ({', '.join(column_names)}) VALUES(?, ?, ?)",
        ("debate", word, now.isoformat()),
    )
    c.execute(
        f"INSERT INTO topics ({', '.join(column_names)}) VALUES(?, ?, ?) ON CONFLICT DO NOTHING",
        ("debate", word, now.isoformat()),
    )
    # "named" placeholder
    c.execute(
        "UPDATE topics SET reason =(:reason) WHERE value =(:value)",
        {"reason": "Because I felt like it", "value": "hello"},
    )
conn.commit()
for cmd in conn.iterdump():
    print(cmd)

sometime_in_the_recent_past = now - datetime.timedelta(seconds=2)
for comparison, description in (("<=", "Old stuff"), (">", "New stuff")):
    query = f"SELECT * from topics where timestamp {comparison} ?"
    c.execute(query, [sometime_in_the_recent_past.isoformat()])
    print(description)
    print(tabulate.tabulate(c, headers=column_names))
