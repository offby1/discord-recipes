# https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Basic_method

def fast_exponent(base: int, power: int) -> int:
    print(f"{base=} {power=}")
    if power < 0:
        return fast_exponent(1 / base, -power)

    elif power == 0:
        return 1

    elif power == 1:
        return base

    else:
        q, r = divmod(power, 2)
        from_recursive_call = fast_exponent(base * base, q)

        if r:
            return base * from_recursive_call
        else:
            return from_recursive_call
