# https://en.wikipedia.org/wiki/Bell_triangle#Construction
import pprint

triangle = [[1]]


def new_row(previous_row):
    row = [previous_row[-1]]

    for up_and_left in previous_row:
        left = row[-1]
        row.append(up_and_left + left)
    return row


while len(triangle) < 20:
    pprint.pprint(triangle)
    triangle.append(new_row(triangle[-1]))
