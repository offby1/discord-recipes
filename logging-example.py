# How to set up a formatter and handler

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt="%(asctime)s: %(module)s %(function)s %(message)s", datefmt="%FT%T%z")
handler = logging.FileHandler("my.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

logger.info("Hello world")
