import dataclasses
import pprint


@dataclasses.dataclass
class Point:
    row: int
    column: int


def same_row(p1: Point, p2: Point) -> bool:
    return p1.row == p2.row


def same_column(p1: Point, p2: Point) -> bool:
    return p1.column == p2.column


def same_diagonal(p1: Point, p2: Point) -> bool:
    row_delta = abs(p1.row - p2.row)
    column_delta = abs(p1.column - p2.column)
    return row_delta == column_delta


def test_same_diagonal() -> None:
    p1 = Point(row=0, column=0)
    p2 = Point(row=1, column=1)
    assert same_diagonal(p1, p2)

    p2.row += 1
    assert not same_diagonal(p1, p2)

    p1.column -= 1
    assert same_diagonal(p1, p2)


class Board:
    def __init__(self, num_rows: int, num_cols: int) -> None:
        self.rows: list[list[list[Point]]] = []
        while len(self.rows) < num_rows:
            self.rows.append([[]] * num_cols)

    @property
    def height(self) -> int:
        return len(self.rows)

    @property
    def width(self) -> int:
        return len(self.rows[0])

    def __repr__(self) -> str:
        return pprint.pformat(self.rows)
