import calendar
import datetime
import locale
import os

import tabulate

"""
Three different ways to map e.g. 0 to e.g. Sunday
"""


def number_to_weekday(n):
    if n < 1:
        n += 7
    # January 1, 2018 was a Monday
    return datetime.date(year=2018, month=1, day=n).strftime("%A")


if __name__ == "__main__":
    os.environ['LC_ALL'] = 'ru_RU.UTF-8'
    locale.setlocale(locale.LC_ALL, '')

    def all_flavors():
        for index in range(1, 8):
            one_more = (index) % 7 + 1
            attribute = f'DAY_{one_more}'
            yield (
                calendar.day_name[index - 1],
                locale.nl_langinfo(getattr(locale, attribute)),
                number_to_weekday(index),
            )

    print(
        tabulate.tabulate(
            all_flavors(), headers=('calendar.day_name', 'locale.nl_langinfo', 'number_to_weekday')
        )
    )
