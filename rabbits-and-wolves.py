import collections
import random

RABBIT = object()
WOLF = object()


def animalPicker(animals):
    output = random.sample(animals, k=2)

    if set(output) == {RABBIT, WOLF}:
        animals.remove(RABBIT)
        animals.append(WOLF)
    elif set(output) == {RABBIT}:
        animals.append(RABBIT)
    else:
        for _ in output:
            animals.remove(WOLF)


def main():
    generation = 1
    animals = [RABBIT, WOLF] * 30
    while len(animals) > 1:
        animalPicker(animals)

        rabbit_count = animals.count(RABBIT)
        wolf_count = animals.count(WOLF)

        generation += 1

        if wolf_count == 0:
            break  # overpopulation imminent

    return {"wabbits": rabbit_count, "wooves": wolf_count}


outcomes = collections.Counter()
for i in range(1000):
    outcomes.update(main())

print(outcomes)
