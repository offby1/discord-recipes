from itertools import count

import matplotlib.pyplot as plt
import pandas as pd


def collatz(n):
    for counter in count():
        q, r = divmod(n, 2)
        if r == 0:
            n = q
        else:
            n = (n * 3) + 1

        if n == 1:
            return counter


df = pd.DataFrame([(i, collatz(i)) for i in range(1, 100)])

plt.bar(df[0], df[1])
plt.show()
