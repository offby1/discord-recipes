# Someone's homework -- a caesar-like cipher, where the offset increases by one for each letter in the plaintext.

import itertools
import string


def n_past_letter(letter, n, reversed=False):
    if n == 0:
        return letter

    if reversed:
        alphabet = itertools.cycle(string.ascii_lowercase[::-1])
    else:
        alphabet = itertools.cycle(string.ascii_lowercase)

    # "rotate" the alphabet until our letter is first
    while True:
        if letter == next(alphabet):
            break

    # now rotate N times more
    while True:
        candidate = next(alphabet)
        n -= 1
        if n == 0:
            break

    return candidate


def weirdo_caesar_encrypt(text, encrypt=True):
    result = []
    for offset, ch in enumerate(text):
        if ch not in string.ascii_lowercase:
            result.append(ch)
        else:
            result.append(n_past_letter(ch, offset, reversed=not encrypt))

    return ''.join(result)


if __name__ == "__main__":
    wat = weirdo_caesar_encrypt("Some random ciphertext that, one hopes, will round-trip successfully")
    print(wat)
    print(weirdo_caesar_encrypt(wat, encrypt=False))
