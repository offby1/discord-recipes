def do_one_flip_in_place(seq):
    for index, elt in enumerate(seq[:-1]):
        if elt > seq[index + 1]:
            print(seq)
            print("[", end='')
            print(', '.join(['^' if i == index + 1 else ' ' for i in range(len(seq))]), end='')
            print("]")
            seq[index + 1], seq[index] = seq[index], seq[index + 1]
            return True

    return False


def bubble_sort(seq):
    while do_one_flip_in_place(seq):
        pass


if __name__ == "__main__":
    import random

    seq = list(range(10))
    random.shuffle(seq)
    print(seq)
    bubble_sort(seq)
    print(seq)
