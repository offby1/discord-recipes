import dataclasses
import re
from typing import List


@dataclasses.dataclass
class Entry:
    word: str
    line_number: int
    predecessor_words: List[str] = dataclasses.field(default_factory=list)
    successor_words: List[str] = dataclasses.field(default_factory=list)

    def context_words(self):
        return tuple(self.predecessor_words + [self.word] + self.successor_words)

    def __str__(self):
        l_ = []
        if self.predecessor_words:
            preds = '.'.join(self.predecessor_words)
        else:
            preds = ''
        padding_needed = max(60 - len(preds), 0)
        l_.append(' ' * padding_needed + preds)
        l_.append(self.word)
        if self.successor_words:
            l_.append('.'.join(self.successor_words))

        return ' '.join(l_) + f" line {self.line_number}"


def entries_from_lines(lines):
    for line_number, line in enumerate(lines):
        line_number += 1
        line = line.strip()
        words = [s for s in re.split(r'[^A-Za-z]', line) if s]

        for index in range(len(words)):
            if words[index]:
                e = Entry(word=words[index], line_number=line_number)

                e.predecessor_words = words[0:index]

                e.successor_words = words[index + 1 :]

                yield e


def concordance_from_file(filename):
    raw_concordance = []

    with open(filename) as inf:
        for entry in entries_from_lines(inf):
            raw_concordance.append(entry)

    return sorted(raw_concordance,
                  key=lambda entry:
                  (entry.word.lower(),
                   [w.lower() for w in entry.predecessor_words],
                   [w.lower() for w in entry.successor_words]
                   ))


if __name__ == "__main__":
    for e in concordance_from_file('/tmp/king-james-bible.txt'):
        print(e)
