import collections
import functools

import pytest
import sympy.ntheory
from hypothesis import given, settings
from hypothesis import strategies as st


@functools.cache
def smaller_factors_of(n):
    candidate = 2

    while True:
        if candidate * candidate > n:
            return

        if n % candidate == 0:
            yield candidate

        candidate += 1


def prime_generator():
    candidate = 2

    while True:
        if next(smaller_factors_of(candidate), None) is None:
            yield candidate

        if candidate == 2:
            candidate += 1
        else:
            candidate += 2


def prime_factors_of(n):
    print(f"Factoring {n=:,}")

    if n < 2:
        return

    for candidate in prime_generator():
        q, r = divmod(n, candidate)

        if r == 0:
            # Candidate is a prime factor of n.
            yield candidate

            # Call ourselves recursively on n / candidate, to get those factors too.
            yield from prime_factors_of(q)
            return

        if candidate * candidate > n:
            break

    # We found no prime factors (or else we'd have returned, above).
    # So n is prime.
    yield n
    return


@pytest.fixture(autouse=True)
def report_stats(capsys):
    yield
    with capsys.disabled():
        print(smaller_factors_of.cache_info())


# run tests like this
# `poetry run python3 -m pytest  --hypothesis-show-statistics fast-simple-correct.py `
@settings(deadline=None)
@given(st.integers(max_value=pow(10, 12)))
def test_just_some_big_ol_random_number(i):
    actual = dict(collections.Counter(prime_factors_of(i)))
    expected = sympy.ntheory.factorint(i)

    if i <= 0:  # special case where sympy returns something wacky
        expected = {}

    assert expected == actual


if __name__ == "__main__":
    pg = prime_generator()

    while True:
        p = next(pg)
        if p >= 100_000:
            break
        print(p)
